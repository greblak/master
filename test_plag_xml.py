import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import xml.dom.minidom


panDir = "pan_test"
paths = [panDir+"\\02-no-obfuscation",panDir+"\\03-random-obfuscation",panDir+"\\04-translation-obfuscation",panDir+"\\05-summary-obfuscation"]
impl = xml.dom.minidom.getDOMImplementation()
rats = []

for path in paths:
	if "no-obfuscation" not in path:
		continue
	files = os.listdir(path)
	for fi in files:
		if "xml" not in fi:
			continue

		print(fi)
		doc = xml.dom.minidom.parse(path+"/"+fi)
		# print("Got doc!")

		pl_su = []
		pl_sr = []
		first_pla = 0
		pla = {}



		features = doc.getElementsByTagName("feature")
		for f in features:
			if(f.getAttribute("name") == "plagiarism"):
				src_off = int(f.getAttribute("source_offset"))
				src_len = int(f.getAttribute("source_length"))
				susp_off = int(f.getAttribute("this_offset"))
				susp_len = int(f.getAttribute("this_length"))
				first_pla = susp_off
				# obfuscation = f.getAttribute("obfuscation")
				# if(obfuscation == "" or obfuscation == None):
					# obfuscation = f.getAttribute("type")
				# if len(obfuscation) <= 1:
					# obfuscation = "NoPlagiarism_Summary"
				for n in range(susp_off, susp_off+susp_len):
					pla[n] = list(range(src_off, src_off+src_len))
		if len(features) < 1 or len(pla) < 1:
			continue

		pla_l = {}

		rat = 0
		ints = 0
		min_src = sys.maxsize
		min_susp = sys.maxsize
		max_src = 0
		max_susp = 0
		with open("./out_test/stat.csv" , 'r') as csvfile:
			seeds = csv.reader(csvfile, delimiter=',', quotechar='"')

			# print(seeds)
			for s in seeds:

				dec = {}

				if "srcName" in s[0] or "Pair" in s[0]:
					continue

				r = re.match(r'.*(suspicious-document[0-9]{5}).*',s[0],re.M|re.I)
				suspName = r.group(1)
				r = re.match(r'.*(source-document[0-9]{5}).*',s[0],re.M|re.I)
				srcName = r.group(1)
				# print(fi)
				if suspName not in fi or srcName not in fi:
					continue

				if "b'NoPlagiarism" in s[10] :
					# print("Aint there")
					continue

				# r = re.match(r'.*(suspicious-document[0-9]{5}).*',s[0],re.M|re.I)
				# suspName = r.group(1)
				# r = re.match(r'.*(source-document[0-9]{5}).*',s[0],re.M|re.I)
				# srcName = r.group(1)

				se = s[12:16]
				print(se)
				# print(se)
				min_src = min(min_src, int(se[0]))
				min_susp = min(min_susp, int(se[1]))

				max_src = max(max_src, int(se[0])+int(se[2]))
				max_susp = max(max_susp, int(se[1])+int(se[3]))
				for n in range(int(se[1]), int(se[1])+int(se[3])):
					dec[n] = list( range(int(se[0]), int(se[0])+int(se[2])))
				# print(len(dec))
				for su in pla:
					if su not in dec:
						continue
					for sr in pla[su]:
						if sr in dec[su]:
							ints+=1
							if su not in pla_l:
								pla_l[su] = []
							pla_l[su].append(sr)
			# print(ints)
			# print((len(pla_l)*len(pla_l[first_pla])))
			# print((len(pla)*len(pla[first_pla])))
			rat = ints/(len(pla)*len(pla[first_pla]))
			# print(rat)
			rat = (len(pla_l)*len(pla_l[first_pla]))/(len(pla)*len(pla[first_pla]))
			print(rat)
			print(min_src)
			sr_len = max_src-min_src
			print(sr_len)
			print(min_susp)
			su_len = max_susp-min_susp
			print(su_len)
			# print(pla)
			rats.append(rat)

	rat = sum(rats) / len(rats)
	print(rat)