python 1_scanAndMetrics.py pan_model out_model
python 2_statistics.py pan_model out_model
python 1_scanAndMetrics.py pan_test out_test
python 2_statistics.py pan_test out_test

REM DATED
REM python 3_dataSplit.py


D:\prog\KNIME_2.11.1\knime.exe -launcher.suppressErrors -noexit -nosplash -reset -application org.knime.product.KNIME_BATCH_APPLICATION -workflowDir="D:/prog/knime_2.9.2/workspace/Statistics"
:waitLoopA
if exist {oout/seeding_predictions_model.csv}(
) else (
REM wait 10 minutes
	timeout 600
	goto waitLoopA
)
REM python 4_distance.py out_test/seeding_predictions_test.csv out_test/extensions_test.csv
REM python 4_distance.py out_model/seeding_predictions_model.csv out_model/extensions_model.csv

python .\4_extension.py .\out_test\seeding_predictions_test.csv out_test/extension.csv pan_test
python .\4_extension.py .\out_model\seeding_predictions_model.csv out_model/extension.csv pan_model

D:\prog\KNIME_2.11.1\knime.exe -launcher.suppressErrors -noexit -nosplash -reset -application org.knime.product.KNIME_BATCH_APPLICATION -workflowDir="D:/prog/knime_2.9.2/workspace/Classification"
:waitLoopB
if exist {oout/finished_classification.csv}(
) else (
REM wait 10 minutes
	timeout 600
	goto waitLoopB
)

python 5_filtering.py pan_test out_test
python 6_xmlOutput_new.py pan_test out_test
clear
D:\Prog\WinPython-32bit-2.7.9.3\python-2.7.9\winpy performance.py -p .\pan_test\02-no-obfuscation -d out_test/xml/none
D:\Prog\WinPython-32bit-2.7.9.3\python-2.7.9\winpy performance.py -p .\pan_test\03-random-obfuscation -d out_test/xml/random
D:\Prog\WinPython-32bit-2.7.9.3\python-2.7.9\winpy performance.py -p .\pan_test\04-translation-obfuscation -d out_test/xml/translation
D:\Prog\WinPython-32bit-2.7.9.3\python-2.7.9\winpy performance.py -p .\pan_test\05-summary-obfuscation -d out_test/xml/summary
D:\Prog\WinPython-32bit-2.7.9.3\python-2.7.9\winpy performance.py -p pan_test -d out_test/xml

python case_doc_performance.py pan_test\02-no-obfuscation out_test/xml/none
python case_doc_performance.py pan_test\03-random-obfuscation out_test/xml/random
python case_doc_performance.py pan_test\04-translation-obfuscation out_test/xml/translation
python case_doc_performance.py pan_test\05-summary-obfuscation out_test/xml/summary
python case_doc_performance.py