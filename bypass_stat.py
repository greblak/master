# g.write("{},{},{},{},{},{},{},{},|{}|,|{}|,{},{},{},{}\n".format(self.srcDoc, self.suspDoc, c,r, cos_mes, dice_co, potentialPlagType.encode(sys.stdout.encoding,errors='replace'),plagType.encode(sys.stdout.encoding, errors='replace'), "","",self.src_offsets[r][0], self.susp_offsets[c][0],self.src_offsets[r][1],self.susp_offsets[c][1]))
import math
import statistics
import time
import codecs
import re
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy

with open(sys.argv[2]+'/cosData.csv', 'r') as csvfile:

	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	rowNum = 0

	with open(sys.argv[2]+"/stat.csv", 'a') as f:
		# Write header to output file.
		f.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen","distFromMaxCos","distFromMaxDice","distFromMeanCos","distFromMeanDice","vertDistCos","vertDistDice"))

		currentPairData = []
		currentPairName = ""
		i = 0
		t = time.time()
		start_time = time.time()

		for row in csvr:
			i+=1
			src = row[0]
			susp = row[1]

			suspSenNum = int(row[2])
			srcSenNum = int(row[3])
			cos = float(row[4])
			dice = float(row[5])
			potentialPlagType = row[6]
			plagType = row[7]
			src_sen = row[8]
			susp_sen = row[9]
			src_offset = row[10]
			susp_offset = row[11]
			src_length = row[12]
			susp_length = row[13]
			pairName = "Susp: {} - Src:{}".format(susp,src).replace('\n', '')
			f.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(pairName, srcSenNum, suspSenNum,plagType,cos, "False", 0,dice, "False", 0,potentialPlagType, potentialPlagType, src_offset, susp_offset, src_length,susp_length,0,0,0,0,0,0))
			if i % 10000 == 0:
				print(i)