import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import xml.dom.minidom


panDir = "pan_test"
paths = [panDir+"\\02-no-obfuscation",panDir+"\\03-random-obfuscation",panDir+"\\04-translation-obfuscation",panDir+"\\05-summary-obfuscation"]
impl = xml.dom.minidom.getDOMImplementation()
cov = []
for path in paths:
	files = os.listdir(path)
	for fi in files:
		if "xml" not in fi:
			continue

		print(fi)
		doc = xml.dom.minidom.parse(path+"/"+fi)
		# print("Got doc!")

		pl_su = []
		pl_sr = []


		features = doc.getElementsByTagName("feature")
		if len(features) < 1:
			continue
		for f in features:
			if(f.getAttribute("name") == "plagiarism"):
				src_off = int(f.getAttribute("source_offset"))
				src_len = int(f.getAttribute("source_length"))
				susp_off = int(f.getAttribute("this_offset"))
				susp_len = int(f.getAttribute("this_length"))
				# obfuscation = f.getAttribute("obfuscation")
				# if(obfuscation == "" or obfuscation == None):
					# obfuscation = f.getAttribute("type")
				# if len(obfuscation) <= 1:
					# obfuscation = "NoPlagiarism_Summary"
				for n in range(susp_off, susp_off+susp_len):
					pl_su.append(n)
				for n in range(src_off, src_off+src_len):
					pl_sr.append(n)
		num_su = len(pl_su)
		num_sr = len(pl_sr)
		if num_su < 1:
			continue
		# print(num_su)
		# print(num_sr)

		with open("out_test/finished_classification.csv" , 'r') as csvfile:
			extensions = csv.reader(csvfile, delimiter=',', quotechar='"')

			for s in extensions:
				# print(s)
				# print(s[0:2])
				if s[0] not in fi or s[1] not in fi:
					continue
				else:
					print(s[0:2])

				# if "True" not in s[20] :
				# 	print("Aint there")
				# 	continue
				#
				# print(fi)
				if s[0] not in fi or s[1] not in fi:
					continue
				se = s[2:6]
				se_src = range(int(se[0]), int(se[0])+int(se[2]))
				se_susp = range(int(se[1]), int(se[1])+int(se[3]))
				int_src = set(se_src).intersection(pl_sr)
				int_susp = set(se_susp).intersection(pl_su)
				for s in int_src:
					pl_sr.remove(s)
				for s in int_susp:
					pl_su.remove(s)
		cover_susp = 0
		cover_src = 0
		if num_su > 0:
			cover_susp = 1-len(pl_su)*1./num_su
		if num_sr > 0:
			cover_src = 1-len(pl_sr)*1./num_sr
		# if len(pl_su) > 5:
		print(len(pl_su))
		print(len(pl_sr))
		cov.append(cover_susp)
		cov.append(cover_src)
		# print("Coverage {}".format(fi))
		# print(cover_src)
		# print(cover_susp)

avg = sum(cov)/len(cov)
print("AVG")
print(avg)


