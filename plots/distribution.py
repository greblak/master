import csv
import matplotlib.pyplot as plt

with open('../oout/bowData.csv', 'r') as csvfile:
	data = {'NoPlagiarism;': [],'none;': [],'random;': [],'translation-chain;': [],'summary;': []}
	for t in data:
		for i in range(0,101):
			data[t].append(0)
	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	rowNum = 0
	numTotal = 100000000
	for row in csvr:
		rowNum += 1
		if rowNum % int(numTotal/100) == 0:
			print(100.*rowNum/numTotal)
		if rowNum > numTotal:
			break
		cos = row[4]
		dice = row[5]
		jacc = row[6]
		plag = row[7]
		plagType = row[8]
		bucket = int(float(cos)*100)
		data[plagType][bucket] = data[plagType][bucket] + 1

	for plagType in data:
		print("Plagtype: {}".format(plagType))
		print("0: {}".format(data[plagType][0]))
		print("100: {}".format(data[plagType][100]))
		print("Local Max: {}".format(max(data[plagType][1:-1])))
		print("Distribution: {}%".format(100.*sum(data[plagType])/numTotal))
	print("Total: {}".format(numTotal))


	# plt.plot(data['NoPlagiarism;'], color='red', label="No Plagiarism")
	plt.plot(data['none;'], color='blue', label="No obfuscation")
	plt.plot(data['random;'], color='green', label="Random")
	plt.plot(data['translation-chain;'], color='purple',label="Translation")
	plt.plot(data['summary;'], color='orange', label="Summary")
	plt.legend()
	plt.xlabel("Cosine similarity *100")
	plt.ylabel("Occurrences")
	plt.show()

