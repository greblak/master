import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy


with open('../out_test/cosData.csv', 'r') as csvfile:

	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	rowNum = 0
	numTotal = 1000000
	data = {}
	pair = {}
	i = 0
	# data[pairName][srcSen][suspSen] = cos ---- Desired
	# data[pairName]
	for row in csvr:
		# print(row)
		src = row[0]
		susp = row[1]
		suspSenNum = int(row[2])
		srcSenNum = int(row[3])
		cos = float(row[4])
		dice = float(row[5])
		plagType = row[6]
		src_sen = row[7]
		susp_sen = row[8]
		src_offset = row[10]
		if src_offset == "":
			print(row)
		susp_offset = row[11]
		src_len = row[12]
		susp_len = row[13]
		if srcSenNum not in pair or len(pair[srcSenNum]) is 0:
			pair[srcSenNum] = {}
		pair[srcSenNum][suspSenNum] = [cos,plagType,src_sen, susp_sen,src_offset,susp_offset,src_len,susp_len]
		if i % 10000 is 0:
			print("{}%".format(int(i*100./numTotal)))
		if i > numTotal:
			break
		i+=1
		pairName = "Susp: {} - Src:{}".format(susp,src)
		if pairName not in data:
			pair = {}
		# print(pair)
		data[pairName] = pair

	i = 0
	# print(data.keys())
	for pairName in data:
		print(pairName)
		pairs = data[pairName]
		# print(len(pairs))


		if(len(pairs) > 50): #or "00069" not in pairName: #For development TODO: REMOVE
			continue
		heatmap = []
		plagmap = []
		plagTypes = []

		maxMap = []
		for srcSen in pairs:
			heatmap.append([])
			plagmap.append([])
			maxMap.append([0.]*len(pairs[srcSen]))
			for suspSen in pairs[srcSen]:
				heatmap[srcSen].append(pairs[srcSen][suspSen][0])
				if  "NoPlagiarism" not in pairs[srcSen][suspSen][1]:
					plagmap[srcSen].append(1.)
					print(pairs[srcSen][suspSen][4:])
					# print("Score!")chrome
				else:
					plagmap[srcSen].append(0.)
				if pairs[srcSen][suspSen][1] not in plagTypes:
					plagTypes.append(pairs[srcSen][suspSen][1] )

		for susp in range(0,len(heatmap[0])):
			# print(susp)
			m = max(numpy.array(heatmap)[:,susp])
			# print(m)
			indicies = numpy.where(numpy.array(heatmap)[:,susp] == m)
			for i in indicies:
				try:
					if m > 0.0:
						maxMap[i][susp] = m
				except TypeError:
					pass
		if len(plagTypes) is 1:
			continue
		fig, (ax, ax1, ax2) = plt.subplots(3, sharex=True, sharey=True)
		# print(plagmap)
		# print(len(heatmap))
		ax.imshow(heatmap, cmap=plt.cm.Blues, interpolation='nearest')
		ax1.imshow(maxMap, cmap=plt.cm.Greens, interpolation='nearest')
		ax2.imshow(plagmap, cmap=plt.cm.Reds, interpolation='nearest')
		ax.set_title("Cosine Measure Heatmap\n{}\nExpected: {}".format(pairName,plagTypes))
		ax1.set_title("Max Cosine Value")
		ax2.set_title("Expected plagiarism")
		ax.invert_yaxis()
		ax1.invert_yaxis()
		ax1.invert_yaxis()
		# Move left and bottom spines outward by 10 points
		ax.spines['left'].set_position(('outward', 10))
		ax1.spines['left'].set_position(('outward', 10))
		ax2.spines['left'].set_position(('outward', 10))
		ax2.spines['bottom'].set_position(('outward', 10))
		# Hide the right and top spines
		ax.spines['right'].set_visible(False)
		ax.spines['bottom'].set_visible(False)
		ax1.spines['right'].set_visible(False)
		ax1.spines['bottom'].set_visible(False)
		ax.set_ylabel("Source sentence")
		ax1.set_ylabel("Source sentence")
		ax2.set_ylabel("Source sentence")
		ax2.set_xlabel("Suspicious sentence")
		# Only show ticks on the left and bottom spines
		ax.yaxis.set_ticks_position('left')
		ax.xaxis.set_ticks_position('bottom')
		ax.set_ylim([0, len(heatmap)])
		ax1.set_ylim([0, len(heatmap)])
		ax2.set_ylim([0, len(heatmap)])
		# fig.subplots_adjust(hspace=2)
		plt.show()


