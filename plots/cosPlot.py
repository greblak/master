import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy

with open('../oout/cosData.csv', 'r') as csvfile:

	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	rowNum = 0
	numTotal = 1000000
	data = {}
	pair = {}
	i = 0
	# data[pairName][srcSen][suspSen] = cos ---- Desired
	# data[pairName]
	for row in csvr:

# self.srcDoc, self.suspDoc, c,r, cos_mes, dice_co, potentialPlagType.encode(sys.stdout.encoding,errors='replace'),plagType.encode(sys.stdout.encoding, errors='replace'), src_sent.encode(sys.stdout.encoding, errors='replace'), susp_sent.encode(sys.stdout.encoding, errors='replace'),self.src_offsets[r][0], self.susp_offsets[c][0],self.src_offsets[r][1],self.susp_offsets[c][1])

		src = row[0]
		susp = row[1]
		suspSenNum = int(row[2])
		srcSenNum = int(row[3])
		cos = float(row[4])
		dice = float(row[5])
		plagType = row[6]
		src_sen = row[7]
		susp_sen = row[8]
		src_offset = row[9]
		if srcSenNum not in pair or len(pair[srcSenNum]) is 0:
			pair[srcSenNum] = {}
		pair[srcSenNum][suspSenNum] = [cos,plagType,src_sen, susp_sen,src_offset]
		if i % 10000 is 0:
			print("{}%".format(int(i*100./numTotal)))
		if i > numTotal:
			break
		i+=1
		pairName = "Susp: {} - Src:{}".format(susp,src)
		if pairName not in data:
			pair = {}
		# print(pair)
		data[pairName] = pair

	i = 0
	# print(data.keys())
	for pairName in data:
		print(pairName)
		pairs = data[pairName]
		print(len(pairs))
		if(len(pairs) > 50): #For development TODO: REMOVE
			continue
		covered = {}
		hasMatches = False
		c = 0
		plagTypes = []
		for srcSen in pairs:
			for suspSen in pairs[srcSen]:
				# print("PAIR: {} - {}".format(srcSen,suspSen))
				c+=1
				x_all = []
				y_all = []
				
				x_matches = []
				y_matches = []
				sent_src = []
				sent_susp = []
				m = min(len(pairs.keys())-srcSen, len(pairs[srcSen].keys())-suspSen)-1
				i = 0
				plagType = "NoPlagiarism"
				while srcSen+i in pairs.keys() and suspSen+i in pairs[srcSen+i].keys() and (srcSen+i not in covered or suspSen+i not in covered[srcSen+i]):
					if srcSen+i not in covered:
						covered[srcSen+i] = []
					if suspSen + i not in covered[srcSen+i]:
						covered[srcSen+i].append(suspSen+i)
					plagType = pairs[srcSen+i][suspSen+i][1]
					y = pairs[srcSen+i][suspSen+i][0]
					x = pairs[srcSen+i][suspSen+i][4]
					if "NoPlagiarism" not in pairs[srcSen+i][suspSen+i][1]:
						if plagType not in plagTypes:
							plagTypes.append(plagType)
						# print("sub: {} - {}".format(srcSen+i,suspSen+i))
						# print(plagType)
						# print(pairs[srcSen+i][suspSen+i][0])
						# print(pairs[srcSen+i][suspSen+i][2].encode(sys.stdout.encoding, errors='replace'))
						# print(pairs[srcSen+i][suspSen+i][3].encode(sys.stdout.encoding, errors='replace'))
						hasMatches = True
						y_matches.append(x)
						x_matches.append(y)
					y_all.append(y)
					x_all.append(x)
					i+=1

				plt.plot(x_all,y_all, color="grey", linewidth = .25) #Print all combinations as gray background
				plt.plot(x_matches,y_matches, color="red", linewidth = 1) #Overlay good hits with color
				plt.scatter(x_matches,y_matches, color="red")

		print(plagTypes)
		if hasMatches:
			plt.ylabel("Similarity")
			plt.xlabel("Source position")
			axes = plt.gca()
			axes.set_ylim([0,1.1])
			plt.title(pairName)
			plt.show()
			hasMatches = False

		# print("Count {}".format(c))
		# plt.clf()

