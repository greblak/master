#
# OBSOLETE
#
import codecs 
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy

if len(sys.argv) == 3:
	# t1=time.time()
	input_file = sys.argv[1]
	output_file = sys.argv[2]
else:
	print("Expecting arguments: input_file output_file")
	exit()
with open(input_file, 'r') as csvfile:

	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	# ['"Pair"', '"SrcSenOff"', '"SuspSenOff"', 
	# 3 '"PlagType"', '"Cos"', '"IsMaxCos"', '"NeighbourMaxCos"', '"Dice"', 
	# 8 '"IsMaxDice"', '"NeighbourMaxDice"', '"PotentialPlagType"', '"LikelyPlag;"', 
	# 12 "SrcOff", "SuspOff", "SrcLen", "SuspLen"]
	# 16 '"IsPlag"', '"Prediction (LikelyPlag;)"', 

	pairsName = ""
	pairs = {}
	with open(output_file, 'a') as f:
		f.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen",  "distFromMaxCos","distFromMaxDice","distFromMeanCos","distFromMeanDice","IsPlag","cluster","Prediction", "DistanceToNearestPrediction",))
		writer = csv.writer(f, lineterminator='\n')
		for row in csvr:
			if "Pair" in row[0]:
				continue
			if pairsName == "":
				pairsName = row[0] # Initialize
				pairs = []

			if row[0] != pairsName:
				# Calculate distance
				print(pairsName)
				if "Pair" in pairsName:
					continue
				srcSuspPair = {}
				for r in pairs: # Fill srcSuspPair[src][susp]
					if int(r[1]) not in srcSuspPair:
						srcSuspPair[int(r[1])] = {}
					if int(r[0]) not in srcSuspPair[int(r[1])]:
						srcSuspPair[int(r[1])][int(r[0])] = r	

				pairs.sort(key=lambda x: int(x[11]))
				for susp in srcSuspPair:
					distance = sys.maxsize
					for su in srcSuspPair:
						for sr in srcSuspPair[su]:
							r = srcSuspPair[su][sr]

							if "False" in str(r[16]):
								continue # Of no interest. Continue

							d = math.fabs(int(su) - int(susp))
						
							if d < distance and d > 0:
								distance = d
							
								# |Pair|,|SrcSenOff|,|SuspSenOff|,|PlagType|,|Cos|,|IsMaxCos|,|NeighbourMaxCos|,|Dice|,|IsMaxDice|,|NeighbourMaxDice|,|
								# PotentialPlagType|,|LikelyPlag|,|SrcOffset|,|SuspOffset|,|SrcLen|,|SuspLen|,|IsPlag|,|Prediction (LikelyPlag)|
					for src in sorted(srcSuspPair[susp]):
						# if "False" not in srcSuspPair[susp][src][16]: # or ("False" not in srcSuspPair[susp][src][4] or "False" not in srcSuspPair[susp][src][7]):							
						# print("{} - {}".format(susp,distance ))
						srcSuspPair[susp][src].insert(0, "|{}|".format(pairsName)) 
						# srcSuspPair[susp][src][1] = "|{}|".format(srcSuspPair[susp][src][1])
						# srcSuspPair[susp][src][2] = "|{}|".format(srcSuspPair[susp][src][2])
						srcSuspPair[susp][src].pop(1)
						srcSuspPair[susp][src].pop(1)
						# print(srcSuspPair[susp][src][-1])
						srcSuspPair[susp][src].append(distance)
						# print(len(srcSuspPair[susp][src]))
						writer.writerow(srcSuspPair[susp][src])

					# if (distance < 2 and debug ) and (int(srcSuspPair[susp][src][11]) > 5800 and int(srcSuspPair[susp][src][11]) < 5900 and debug):
					# 	print("{} {} {}".format(srcSuspPair[susp][src][12],srcSuspPair[susp][src][11],distance))
				# Reset
				pairs = []
				pairsName = row[0]
			else:
				pairs.append(row[1:])



	'''	i = 0
		for pair in pairs:
			
'''

					