import sys
import os
import statistics 
import codecs
import re
import time
import csv
import numpy as numpy


with open(sys.argv[1]+'/stat.csv', 'r') as csvfile:
	#Generate pair list
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	pairs = []
	with open(sys.argv[1]+'/stat_fixed.csv', 'a') as f:
		outWriter = csv.writer(f, lineterminator='\n')
		for row in csvr:
			pair = row[0]
			likelyPlag = row[11]

			if "Pair" in pair or likelyPlag == "False" or (likelyPlag == "True" and row[5] == row[8]):
				outWriter.writerow(row)
				continue
			else:
				r = re.match(r'.*(suspicious-document[0-9]{5}).*',row[0],re.M|re.I)
				suspName = r.group(1)
				r = re.match(r'.*(source-document[0-9]{5}).*',row[0],re.M|re.I)
				srcName = r.group(1)


				# if cos > 0.95 and src_length == susp_length:    
				srcFile = codecs.open(sys.argv[2]+"/src/{}.txt".format(srcName), 'r', 'utf-8').read()
				suspFile = codecs.open(sys.argv[2]+"/susp/{}.txt".format(suspName), 'r', 'utf-8').read()
				suspQuote=suspFile
				srcQuote=srcFile
				print("----------------------")
				offsets = row[12:16] #SrcOff, SuspOff, SrcLen, SuspLen
				print(row[4])
				print("SRC: {}".format(srcQuote[int(offsets[0]):int(offsets[0])+int(offsets[2])]).replace('\n', '').encode(sys.stdout.encoding, errors='replace'))
				print("SUS: {}".format(suspQuote[int(offsets[1]):int(offsets[1])+int(offsets[3])]).replace('\n', '').encode(sys.stdout.encoding, errors='replace'))
				correction = ""
				while correction not in ['y', 'n']:
					correction = input("Plagiarism y/n?")
				if correction == "n":
					row[11] = "False"
				elif correction == "y":
					row[11] = "True"
				else:
					print("WHAT? "+correction)
				outWriter.writerow(row)