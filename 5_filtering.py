import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import flist
import xml.dom.minidom


# From Sanchez-Perez
def sum_vect(dic1,dic2):
	res=dic1
	# print("Keys")
	# print(dic2.keys())
	for i in list(dic2.keys()):
		# i = int(i)
		if i in res:
			res[i]+=dic2[i]
		else:
			res[i]=dic2[i]
	return res
# From Sanchez-Perez
def tf_idf(list_dic1,voc1,list_dic2,voc2):
	idf=sum_vect(voc1,voc2)
	td=len(list_dic1)+len(list_dic2)
	for i in range(len(list_dic1)):
		for j in list(list_dic1[i].keys()):
			list_dic1[i][j]*=math.log(td/float(idf[j]))
	for i in range(len(list_dic2)):
		for j in list(list_dic2[i].keys()):
			list_dic2[i][j]*=math.log(td/float(idf[j]))

# From Sanchez-Perez
def eucl_norm(d1):
		norm=0.0
		for val in list(d1.values()):
			norm+=float(val*val)
		return math.sqrt(norm)

# From Sanchez-Perez
def cosine_measure(d1,d2):
	dot_prod=0.0
	det=eucl_norm(d1)*eucl_norm(d2)
	if det==0:
		return 0
	for word in list(d1.keys()):
		if word in d2:
			dot_prod+=d1[word]*d2[word]
	return dot_prod/det

# From Sanchez-Perez
def dice_coeff(d1,d2):
	if len(d1)+len(d2)==0:
		return 0
	intj=0
	for i in list(d1.keys()):
		if i in d2:
			intj+=1
	return 2*intj/float(len(d1)+len(d2))

# From Sanchez-Perez
def ss_treat(list_dic,offsets,min_sentlen,rssent):
	if rssent=='no':
		i=0
		range_i=len(list_dic)-1
		while i<range_i:
			if sum(list_dic[i].values())<min_sentlen:
				list_dic[i+1]=sum_vect(list_dic[i+1],list_dic[i])
				del list_dic[i]
				offsets[i+1]=(offsets[i][0],offsets[i+1][1]+offsets[i][1])
				del offsets[i]
				range_i-=1
			else:
				i=i+1
	else:
		i=0
		range_i=len(list_dic)-1
		while i<range_i:
			if sum(list_dic[i].values())<min_sentlen:
				del list_dic[i]
				del offsets[i]
				range_i-=1
			else:
				i=i+1
# From Sanchez-Perez
def tokenize(text,voc={},offsets=[],sents=[],rem_sw='no'):
	"""
	INPUT:  text: Text to be pre-processed
		   voc: vocabulary used in the text with idf
			offsets: start index and length of each sentence
			sents: sentences of the text without tokenization
	OUTPUT: Returns a list of lists representing each sentence divided in tokens
	"""
	#text.replace('\0x0', ' ')
	sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
	sents.extend(sent_detector.tokenize(text))
	offsets.extend([(a,b-a) for (a,b) in sent_detector.span_tokenize(text)])
	sent_tokens=[nltk.TreebankWordTokenizer().tokenize(sent) for sent in sents]
	stemmer=nltk.stem.porter.PorterStemmer()
	sent_tokens_pp=[]
	stopwords=flist.flist().words()
	cont=0
	for tokens in sent_tokens:
		if rem_sw=='no':
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word)]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		elif rem_sw=='50':
			stopwords=flist.flist().words50()
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word) and word.lower() not in stopwords]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		else:
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word) and word.lower() not in stopwords]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		if len(temp)>0:
			sent_tokens_pp.append(temp)
			for i in list(temp.keys()):
				if i in voc:
					voc[i]+=1
				else:
					voc[i]=1
			cont=cont+1
		else:
			del offsets[cont]
	return sent_tokens_pp


#Joins all possible combinations of groups.
def permute(groups, sortcol):

	out = []
	pairs = []
	for i,v in enumerate(groups):

		out.append(v) #Ensure single groups are added
		# print(v)
		for j,w in enumerate(groups):
			if i == j or j in pairs:
				continue
			g = []
			for n in v:
				g.append(n)
			for n in w:
				g.append(n)
			g.sort(key=lambda x: int(sortcol))
			out.append(g)
		pairs.append(i)
	return out

def gap(group, offcol, lencol):
	group.sort(key=lambda x: int(x[offcol]))
	off_last_off_gap = int(group[0][offcol])
	off_gap = 0
	offs = []
	for g in group:
		o = int(g[offcol])
		offs.append(o)
		l =  int(g[lencol])
		d = 0
		if o > off_last_off_gap:
			d = o - off_last_off_gap
		off_last_off_gap = o + l
		off_gap += d
	return off_gap

def to_passage(group):
	arr = numpy.array(group)

	low_src_off = min(arr[:,11].astype(int))
	high_src_off = max(arr[:,11].astype(int))
	high_src_ind = numpy.where(arr[:,11].astype(int) == high_src_off) #May be multiple values?
	low_susp_off = min(arr[:,12].astype(int))
	high_susp_off = max(arr[:,12].astype(int))
	high_susp_ind = numpy.where(arr[:,12].astype(int) == high_susp_off) #May be multiple values?
	#Calculate length from high values + length for that row
	high_src_ind = int(high_src_ind[0][0])
	high_susp_ind = int(high_susp_ind[0][0])
	# for n in high_src_ind:
	# 	print(n)
	# print(arr[high_src_ind])
	len_src = int(high_src_off) - int(low_src_off) + int(arr[high_src_ind][13])
	len_susp = int(high_susp_off) - int(low_susp_off) + int(arr[high_susp_ind][14])

	susp_off_gap = gap(group, 12, 14)
	src_off_gap = gap(group, 11, 13)

	plagType = "NoPlagiarism"
	for g in group:
		if "NoPlagiarism" not in g[9]:
			plagType = g[9]

	# print("---")
	# print(group)
	# print([low_src_off,low_susp_off,len_src,len_susp,susp_off_gap])
	if susp_off_gap < 0:
		print(offs)
		print(susp_off_gap)
	# if(low_susp_off == 8529):
	# 	print("HERE I AM")
	# 	print("HERE I AM")
	# 	print("HERE I AM")
	# 	print("HERE I AM")
	# 	print(len(group))
	# print((low_src_off,low_susp_off,len_src,len_susp,susp_off_gap,src_off_gap))
	return (low_src_off,low_susp_off,len_src,len_susp,susp_off_gap,src_off_gap,plagType)

def string_pair(pairName, dataPath,passage):

	# Passage srcOff, suspOff,srcLen, suspLen,suspGap,srcGap,PlagType
	# print(pairName)
	r = re.match(r'.*(suspicious-document[0-9]{5}).*',pairName,re.M|re.I)
	suspName = r.group(1)
	r = re.match(r'.*(source-document[0-9]{5}).*',pairName,re.M|re.I)
	srcName = r.group(1)
	srcDoc = os.path.join(dataPath+"/src/", srcName+".txt")
	suspDoc = os.path.join(dataPath+"/susp/", suspName+".txt")
	src_fp = codecs.open(srcDoc, 'r', 'utf-8')
	src_text = src_fp.read()#[int(passage[0]):int(passage[0])+int(passage[2])]
	susp_fp = codecs.open(suspDoc, 'r', 'utf-8')
	susp_text = susp_fp.read()#[int(passage[1]):int(passage[1])+int(passage[3])]

	return(src_text,susp_text,srcName,suspName)

def passage_similarity(tokens, passage):

	p = passage
	#tokens
	src_text = tokens[0]
	susp_text = tokens[1]
	#passage conversion
	#(src_off, susp_off, src_len, susp_len)
	src_offsets = []
	susp_offsets = []
	# print(passage)
	# for p in passage:
	# 	src_offsets.append([ p[0],p[2] ])
	# 	susp_offsets.append([ p[1],p[3] ])

	src_voc = {}
	src_sents = []
	src_bow=tokenize(src_text,src_voc,src_offsets,src_sents, "no")
	# ss_treat(src_bow,src_offsets,3,"no")

	susp_voc = {}
	susp_sents = []
	susp_bow=tokenize(susp_text,susp_voc,susp_offsets,susp_sents, "no")
	# ss_treat(susp_bow,susp_offsets,3,"no")
	pass_susp_bow = {}
	pass_src_bow = {}
	# print(len(susp_bow))
	# print(len(susp_offsets))

	p_su_o = int(p[1])
	p_su_l = int(p[3])
	p_sr_o = int(p[0])
	p_sr_l = int(p[2])
	susp_gap = int(p[4])
	src_gap = int(p[5])
	plag_type = p[6]
	pass_susp_bow[p_su_o] = []
	pass_src_bow[p_sr_o] = []

	for i,o in enumerate(susp_offsets):
		if int(o[0]) in range(int(p_su_o), int(p_su_o)+int(p_su_l)):
			# print(pass_susp_bow[p_su_o])
			# print(susp_bow[i])
			pass_susp_bow[p_su_o] = sum_vect(dict(pass_susp_bow[p_su_o]),susp_bow[i])
	if(len(pass_susp_bow[p_su_o]) == 0):
		print("Empty BOW SUSP")


	for i,o in enumerate(src_offsets):
		# print(o[0])
		if int(o[0]) in range(int(p_sr_o), int(p_sr_o)+int(p_sr_l)):
			pass_src_bow[p_sr_o] = sum_vect(dict(pass_src_bow[p_sr_o]),src_bow[i])
	if(len(pass_src_bow[p_sr_o]) == 0):
		# print("----")
		# print(p_sr_o)
		# print(int(p_sr_l))

		print("Empty BOW SRC")


	if len(pass_src_bow[p_sr_o]) == 0 or len(pass_susp_bow[p_su_o] ) == 0:
		print("What the...")
	cos = cosine_measure(pass_src_bow[p_sr_o],pass_susp_bow[p_su_o])
	dice = dice_coeff(pass_src_bow[p_sr_o],pass_susp_bow[p_su_o])

	next_src = -1
	next_susp = -1
	prev_src_end = -1
	prev_susp_end = -1

	src_end = p_sr_o + p_sr_l -1 #Sub one to get next
	susp_end = p_su_o + p_su_l -1  #Sub one to get next

	for i,v in enumerate(src_offsets):
		o = int(v[0])
		l = int(v[1])
		if int(p_sr_o) in range(o,o+l) and i+1 < len(src_offsets):
			next_src = src_offsets[i+1][0]
		if int(src_end) in range(o,o+l) and i+1 < len(src_offsets):
			prev_src_end = src_offsets[i-1][0]

	for i,v in enumerate(susp_offsets):
		o = int(v[0])
		l = int(v[1])
		if int(p_su_o) in range(o,o+l) and i+1 < len(susp_offsets):
			next_susp = susp_offsets[i+1][0]
		if int(susp_end) in range(o,o+l) and i+1 < len(susp_offsets):
			prev_susp_end = susp_offsets[i-1][0]

	return [p_sr_o,p_su_o,p_sr_l,p_su_l,cos,dice,susp_gap,src_gap,plag_type,src_offsets,susp_offsets,next_src, next_susp, prev_src_end,prev_susp_end]

if len(sys.argv) == 3:
	# t1=time.time()
	pan_path = sys.argv[1]
	test_path = sys.argv[2]
else:
	print(len(sys.argv))
	print("Expecting arguments: input_file output_file pan_path")
	exit()
with open(test_path+"/finished_classification.csv", 'r') as csvfile:

	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='"')
	# ['"Pair"', '"SrcSenOff"', '"SuspSenOff"',
	# 3 '"PlagType"', '"Cos"', '"IsMaxCos"', '"NeighbourMaxCos"', '"Dice"',
	# 8 '"IsMaxDice"', '"NeighbourMaxDice"', '"PotentialPlagType"', '"LikelyPlag;"',
	# 12 "SrcOff", "SuspOff", "SrcLen", "SuspLen"]
	# 16 '"IsPlag"', '"Prediction (LikelyPlag;)"',

	pairsName = ""
	pairCat = []
	pairs = {}
	numPairs = 0
	with open(test_path+"/to_xml.csv", 'a') as f:
		writer = csv.writer(f, lineterminator='\n')
		# f.write("{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("srcDoc","suspDoc","src_off","susp_off","src_len","susp_len","cos","dice","susp_gap","src_gap","PlagType","PlagAcc","IsPlag"))
		for r in csvr:
			if "srcDoc" in r[0]:
				continue
			# Load documents within offsets and create BOW from these.
				# p_sr_o = int(p[0])
				# p_su_o = int(p[1])
				# p_sr_l = int(p[2])
				# p_su_l = int(p[3])
				# susp_gap = int(p[4])
				# src_gap = int(p[5])
				# plag_type = p[6]
				# srcOff, suspOff,srcLen, suspLen,suspGap,srcGap,PlagType
			pairsName = "Src: {} - Susp: {}".format(r[0],r[1])

			passage = [r[2],r[3],r[4],r[5],r[8],r[9],r[10]]

			# print(passage)
			pairTokens = string_pair(pairsName, pan_path,passage)

			p = passage_similarity(pairTokens, passage)
			best_pass = p.copy()
			curr_pass = p.copy()

			# return [p_sr_o,p_su_o,p_sr_l,p_su_l,cos,dice,susp_gap,src_gap,plag_type,src_offsets,susp_offsets,next_src, next_susp, prev_src_end,prev_susp_end]
			# 11: next_src, next_susp, prev_src_end,prev_susp_end
			# dice = curr_pass[5]
			# cos = curr_pass[4]
			# first_dice = dice
			# first_cos = cos
			# print("----")
			# # Minimize src window from front until Dice is decreasing
			# while True:
			# 	src_len = curr_pass[2] - (curr_pass[11] - curr_pass[0])
			# 	if src_len <=0:
			# 		break
			# 	# print(susp_len)

			# 	curr_pass[0] = curr_pass[11]
			# 	curr_pass[2] = src_len
			# 	curr_pass = passage_similarity(pairTokens, curr_pass)
			# 	# print(curr_pass[0:4])
			# 	# print(str(float(curr_pass[5]-dice)))
			# 	# print(curr_pass[4:6])
			# 	if curr_pass[5] > dice or curr_pass[4] > cos :
			# 		dice = curr_pass[5]
			# 		cos = curr_pass[4]
			# 		best_pass = curr_pass
			# 		print("SRF")
			# 	else:
			# 		break
			# # Minimize src window from back until Dice is decreasing
			# while True:
			# 	src_len = curr_pass[13] - curr_pass[0]
			# 	if src_len <=0:

			# 		break
			# 	# print(src_len)
			# 	curr_pass[2] = src_len
			# 	curr_pass = passage_similarity(pairTokens, curr_pass)
			# 	if curr_pass[5] > dice or curr_pass[4] > cos :
			# 		dice = curr_pass[5]
			# 		cos = curr_pass[4]
			# 		best_pass = curr_pass
			# 		print("SRB")
			# 	else:
			# 		break

			# while True:
			# 	# Minimize susp window from back until Dice is decreasing
			# 	susp_len = curr_pass[14] - curr_pass[1]
			# 	if susp_len <=0:
			# 		break
			# 	# print(susp_len)
			# 	curr_pass[3] = susp_len
			# 	curr_pass = passage_similarity(pairTokens, curr_pass)
			# 	if curr_pass[5] > dice or curr_pass[4] > cos :
			# 		# print("SUB")
			# 		dice = curr_pass[5]
			# 		cos = curr_pass[4]
			# 		best_pass = curr_pass
			# 	else:
			# 		# print("Nada")
			# 		break
			# # Minimize susp window from front until Dice is decreasing
			# while True:
			# 	# Minimize susp window from back until Dice is decreasing
			# 	susp_len = curr_pass[3] -(curr_pass[12] - curr_pass[1])
			# 	if susp_len <=0:
			# 		break
			# 	# print(susp_len)
			# 	curr_pass[1] = curr_pass[12]
			# 	curr_pass[3] = susp_len
			# 	curr_pass = passage_similarity(pairTokens, curr_pass)
			# 	if curr_pass[5] > dice or curr_pass[4] > cos :
			# 		dice = curr_pass[5]
			# 		cos = curr_pass[4]
			# 		best_pass = curr_pass
			# 		print("SUF")
			# 	else:
			# 		break


			# #These are the new offsets. Modify row accordingly
			# # if dice>first_dice:
			# # print(float(dice/first_dice))
			# # print(float(cos/first_cos))


			# if(dice > first_dice and cos > first_cos):
			# 	best_pass = best_pass
			# else:
			# 	best_pass = p.copy()
			r[2] = best_pass[0]
			r[3] = best_pass[1]
			r[4] = best_pass[2]
			r[5] = best_pass[3]
			r[6] = best_pass[4]
			r[7] = best_pass[5]
			print(pairsName)
			print(best_pass[0:6])
			print(r[2:7])
			# print(r[2:8])
			# print(p[0:6])
			#After completion


			writer.writerow(r)

	# print(i)
	# print(j)
	print("Numpairs: "+str(numPairs))
	# print(pairCat)