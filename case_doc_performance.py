import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import xml.dom.minidom

panDir = "pan_test"
detDir = "out_test\\xml"
pathsPan = [panDir+"\\02-no-obfuscation",panDir+"\\03-random-obfuscation",panDir+"\\04-translation-obfuscation",panDir+"\\05-summary-obfuscation"]
pathsDet = [detDir+"\\none",detDir+"\\random",detDir+"\\translation", detDir+"\\summary", detDir+"\\BadDetection"]
if len(sys.argv) == 3:
	panDir = sys.argv[1]
	detDir = sys.argv[2]
	pathsPan = [panDir]
	pathsDet = [detDir]
# print(len(sys.argv))

def doc_overlap(case, det):
	if case[0] in det[0]:
		return True
	return False

def is_valid_overlap (case, det):
	if not doc_overlap(case, det):
		return False
	suspDet = range(det[3], det[3]+det[4])
	srcDet = range(det[1], det[1]+det[2])
	suspCase = range(case[3], case[3]+case[4])
	srcCase = range(case[1], case[1]+case[2])
	intSusp = set(suspDet).intersection(suspCase)
	intSrc = set(srcDet).intersection(srcCase)

	if (len(list(intSusp))*len(list(intSrc))) / (len(suspCase)*len(srcCase)) > 0.5:
		return True
	return False



impl = xml.dom.minidom.getDOMImplementation()
rats = []

cases = []
detections = []
for path in pathsPan:
	files = os.listdir(path)
	for fi in files:
		if "xml" not in fi:
			continue
		doc = xml.dom.minidom.parse(path+"/"+fi)

		features = doc.getElementsByTagName("feature")
		for f in features:
			if(f.getAttribute("name") == "plagiarism"):
				src_off = int(f.getAttribute("source_offset"))
				src_len = int(f.getAttribute("source_length"))
				susp_off = int(f.getAttribute("this_offset"))
				susp_len = int(f.getAttribute("this_length"))
				first_pla = susp_off
				cases.append([fi,src_off,src_len,susp_off,susp_len])
		if len(features) < 1:
			continue

for path in pathsDet:
	files = os.listdir(path)
	for fi in files:
		if "xml" not in fi:
			continue
		doc = xml.dom.minidom.parse(path+"/"+fi)

		features = doc.getElementsByTagName("feature")
		for f in features:
			if(f.getAttribute("name") == "detected-plagiarism"):
				src_off = int(f.getAttribute("source_offset"))
				src_len = int(f.getAttribute("source_length"))
				susp_off = int(f.getAttribute("this_offset"))
				susp_len = int(f.getAttribute("this_length"))
				first_pla = susp_off
				detections.append([fi,src_off,src_len,susp_off,susp_len])
		if len(features) < 1:
			continue




truePos = 0
falsePos = 0
falseNeg = 0

for c in cases:
	c_found = False
	for d in detections:
		if is_valid_overlap(c,d):
			c_found = True
	if c_found:
		truePos +=1
	else:
		falseNeg +=1

for d in detections:
	d_found = False
	for c in cases:
		if is_valid_overlap(c,d):
			d_found = True
	if not d_found:
		falsePos += 1

recall = truePos / (truePos + falseNeg)
precision = truePos / (truePos + falsePos)


print("Case")
print(recall)
print(precision)

for c in cases:
	c_found = False
	for d in detections:
		if doc_overlap(c,d):
			c_found = True
	if c_found:
		truePos +=1
	else:
		falseNeg +=1

for d in detections:
	d_found = False
	for c in cases:
		if doc_overlap(c,d):
			d_found = True
	if not d_found:
		falsePos += 1


print("Doc")
recall = truePos / (truePos + falseNeg)
precision = truePos / (truePos + falsePos)

print(recall)
print(precision)