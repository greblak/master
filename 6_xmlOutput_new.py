import operator
import os
import sys
import xml.dom.minidom
import codecs
import re
import math
import csv
import time

# def serialize_features(susp, src, features, outdir):

#['"Pair"', '"PlagType"', '"Cos"', '"IsMaxCos"', '"NeighbourMaxCos"', '"Dice"', '"IsMaxDice"', '"NeighbourMaxDice"', '"PotentialPlagType"',
#'"LikelyPlag"', '"SrcOffset"', '"SuspOffset"', '"SrcLen"', '"SuspLen"', '"IsPlag"', '"Prediction"', '
#"DistanceToNearestPrediction"', '"Prediction (IsPlag)"']
with open(sys.argv[2]+"/to_xml.csv", 'r') as csvfile:

    #print(data)
    csvr = csv.reader(csvfile, delimiter=',', quotechar='"')
    pairsName = ""
    pairs = []
    pairCat = []
    num = 0
    numPairs = 0
    feat = 0

    rows = []
    for r in csvr:
        rows.append(r)
    last = len(rows) -1
    for k,row in enumerate(rows):

        if "srcDoc" in row[0]:
                print(row)
                continue

        # if "00143" not in row[1] and "02739" not in row[0]:
        #     # print(row)
        #     continue

        # print(row[17])
        if "False" in row[9]:# and int(float(row[16])) > 100:
            print(row)
            continue
        if pairsName == "":
            print("init")
            pairsName = "{}-{}".format(row[0],row[1]) # Initialize
            print(pairsName)
            pairs = []

        if "{}-{}".format(row[0],row[1]) not in pairsName or k == last: # New document pair! Calculate and output before resetting
            num+=1
            print("New pair!")
            print(pairsName)
            if pairsName not in pairCat:
                pairCat.append(pairsName)
                numPairs +=1
            #Merge and output to XML
            # print("PN:{}".format(pairsName))
            # print("RR:{}".format(row[0]))
            # pairs.sort(key=lambda x: int(x[11]))
            # pairs = sorted(pairs, key=operator.itemgetter(11)) #Sort by susp offset
            # print(pairs)
            features = [] #[susp_off, susp_len, source_off, source_len]
            r = re.match(r'.*(suspicious-document[0-9]{5}).*',pairsName,re.M|re.I)
            susp = r.group(1)
            r = re.match(r'.*(source-document[0-9]{5}).*',pairsName,re.M|re.I)
            src = r.group(1)

            for i,r in enumerate(pairs):
                src_off = int(r[2])
                susp_off = int(r[3])
                src_len = int(r[4])
                susp_len = int(r[5])
                isPlag = r[9]

                features.append([susp_off,susp_len,src_off,src_len])#,isPlag])

            if len(features) < 1:
                continue
            #Write to XML
            impl = xml.dom.minidom.getDOMImplementation()
            doc = impl.createDocument(None, 'document', None)
            root = doc.documentElement
            root.setAttribute('reference', "{}.txt".format(susp))
            doc.createElement('feature')
            outdir = sys.argv[2]+"/xml/"

            print("Added {} features".format(len(features)))
            feat+=len(features)

            # if len(features) > 10:
            #     print(susp)
            #     print(src)
            # print(features)

            final_features = []

            features.sort(key=lambda x: int(x[0]))  #Sort by susp
            curr_feat = features[0]

            for i in range(1,len(features)-1):
                f = features[i]
                if len(curr_feat) == 0:
                    curr_feat = f
                    continue

                if f[0] in range(curr_feat[0]-1,curr_feat[0]+10):
                    curr_feat[1] = f[0]-curr_feat[0]+f[1]
                    curr_feat[2] = min(curr_feat[2], f[2])
                    curr_feat[3] = max(curr_feat[3], f[2]-curr_feat[2]+f[3])
                else:
                    final_features.append(curr_feat)
                    curr_feat = []
            if len(curr_feat) > 1:
                final_features.append(curr_feat)
            # print(final_features)

            for f in features:
                # print(f)
                feature = doc.createElement('feature')
                feature.setAttribute('name', 'detected-plagiarism')
                feature.setAttribute('this_offset', str(f[0]))
                feature.setAttribute('this_length', str(f[1]))
                feature.setAttribute('source_reference', "{}.txt".format(src))
                feature.setAttribute('source_offset', str(f[2]))
                feature.setAttribute('source_length', str(f[3]))
                # feature.setAttribute('IsPlag', str(f[4]))
                root.appendChild(feature)
            print(len(final_features))

            fileStr = susp + '-'+ src + '.xml'
            plagType = "BadDetection"

            if os.path.isfile(sys.argv[1]+"/02-no-obfuscation/{}".format(fileStr)) :
                plagType = "none"
            elif os.path.isfile(sys.argv[1]+"/03-random-obfuscation/{}".format(fileStr)) :
                plagType = "random"
            elif os.path.isfile(sys.argv[1]+"/04-translation-obfuscation/{}".format(fileStr)) :
                plagType = "translation"
            elif os.path.isfile(sys.argv[1]+"/05-summary-obfuscation/{}".format(fileStr)) :
                plagType = "summary"

            if not os.path.exists("{}{}".format(outdir,plagType)) :
                os.makedirs("{}{}".format(outdir,plagType))
            doc.writexml(open(outdir +plagType+"/"+fileStr, 'w'),encoding='utf-8')

            #Reset
            pairsName = "{}-{}".format(row[0],row[1])
            pairs = []

        pairs.append(row)

    print(numPairs)
    # print(feat)
