import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import flist
import xml.dom.minidom


# From Sanchez-Perez
def sum_vect(dic1,dic2):
	res=dic1
	# print("Keys")
	# print(dic2.keys())
	for i in list(dic2.keys()):
		# i = int(i)
		if i in res:
			res[i]+=dic2[i]
		else:
			res[i]=dic2[i]
	return res
# From Sanchez-Perez
def tf_idf(list_dic1,voc1,list_dic2,voc2):
	idf=sum_vect(voc1,voc2)
	td=len(list_dic1)+len(list_dic2)
	for i in range(len(list_dic1)):
		for j in list(list_dic1[i].keys()):
			list_dic1[i][j]*=math.log(td/float(idf[j]))
	for i in range(len(list_dic2)):
		for j in list(list_dic2[i].keys()):
			list_dic2[i][j]*=math.log(td/float(idf[j]))

# From Sanchez-Perez
def eucl_norm(d1):
		norm=0.0
		for val in list(d1.values()):
			norm+=float(val*val)
		return math.sqrt(norm)

# From Sanchez-Perez
def cosine_measure(d1,d2):
	dot_prod=0.0
	det=eucl_norm(d1)*eucl_norm(d2)
	if det==0:
		return 0
	for word in list(d1.keys()):
		if word in d2:
			dot_prod+=d1[word]*d2[word]
	return dot_prod/det

# From Sanchez-Perez
def dice_coeff(d1,d2):
	if len(d1)+len(d2)==0:
		return 0
	intj=0
	for i in list(d1.keys()):
		if i in d2:
			intj+=1
	return 2*intj/float(len(d1)+len(d2))

# From Sanchez-Perez
def ss_treat(list_dic,offsets,min_sentlen,rssent):
	if rssent=='no':
		i=0
		range_i=len(list_dic)-1
		while i<range_i:
			if sum(list_dic[i].values())<min_sentlen:
				list_dic[i+1]=sum_vect(list_dic[i+1],list_dic[i])
				del list_dic[i]
				offsets[i+1]=(offsets[i][0],offsets[i+1][1]+offsets[i][1])
				del offsets[i]
				range_i-=1
			else:
				i=i+1
	else:
		i=0
		range_i=len(list_dic)-1
		while i<range_i:
			if sum(list_dic[i].values())<min_sentlen:
				del list_dic[i]
				del offsets[i]
				range_i-=1
			else:
				i=i+1
# From Sanchez-Perez
def tokenize(text,voc={},offsets=[],sents=[],rem_sw='no'):
	"""
	INPUT:  text: Text to be pre-processed
		   voc: vocabulary used in the text with idf
			offsets: start index and length of each sentence
			sents: sentences of the text without tokenization
	OUTPUT: Returns a list of lists representing each sentence divided in tokens
	"""
	#text.replace('\0x0', ' ')
	sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
	sents.extend(sent_detector.tokenize(text))
	offsets.extend([(a,b-a) for (a,b) in sent_detector.span_tokenize(text)])
	sent_tokens=[nltk.TreebankWordTokenizer().tokenize(sent) for sent in sents]
	stemmer=nltk.stem.porter.PorterStemmer()
	sent_tokens_pp=[]
	stopwords=flist.flist().words()
	cont=0
	for tokens in sent_tokens:
		if rem_sw=='no':
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word)]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		elif rem_sw=='50':
			stopwords=flist.flist().words50()
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word) and word.lower() not in stopwords]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		else:
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word) and word.lower() not in stopwords]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		if len(temp)>0:
			sent_tokens_pp.append(temp)
			for i in list(temp.keys()):
				if i in voc:
					voc[i]+=1
				else:
					voc[i]=1
			cont=cont+1
		else:
			del offsets[cont]
	return sent_tokens_pp


#Joins all possible combinations of groups.
def permute(groups, sortcol):

	out = []
	pairs = []
	for i,v in enumerate(groups):

		out.append(v) #Ensure single groups are added
		# print(v)
		for j,w in enumerate(groups):
			if i == j or j in pairs:
				continue
			g = []
			for n in v:
				g.append(n)
			for n in w:
				g.append(n)
			g.sort(key=lambda x: int(sortcol))
			out.append(g)
		pairs.append(i)
	return out

def gap(group, offcol, lencol):
	group.sort(key=lambda x: int(x[offcol]))
	off_last_off_gap = int(group[0][offcol])
	off_gap = 0
	offs = []
	for g in group:
		o = int(g[offcol])
		offs.append(o)
		l =  int(g[lencol])
		d = 0
		if o > off_last_off_gap:
			d = o - off_last_off_gap
		off_last_off_gap = o + l
		off_gap += d
	return off_gap

def to_passage(group):
	# for g in group:
	# 	print(g[11:15])
	arr = numpy.array(group)
	low_src_off = min(arr[:,11].astype(int))
	# if low_src_off == 0:
	# 	for e in arr:
	# 		print(e[11:15])
	high_src_off = max(arr[:,11].astype(int))
	high_src_ind_list = numpy.where(arr[:,11].astype(int) == high_src_off) #May be multiple values?
	max_src_len = 0
	high_src_ind = -1
	for r in high_src_ind_list:
		for v in r:
			if int(arr[v][14]) > max_src_len:
				max_src_len = int(arr[v][14])
				high_src_ind = v

	# if(len(high_src_ind) > 1) :
	# 	print("----")
	low_susp_off = min(arr[:,12].astype(int))
	high_susp_off = max(arr[:,12].astype(int))
	high_susp_ind_list = numpy.where(arr[:,12].astype(int) == high_susp_off) #May be multiple values?

	max_susp_len = 0
	high_susp_ind = -1
	for r in high_susp_ind_list:
		for v in r:
			if int(arr[v][14]) > max_susp_len:
				max_susp_len = int(arr[v][14])
				high_susp_ind = v
	#Calculate length from high values + length for that row
	# high_src_ind = int(high_src_ind[0][0])
	# high_susp_ind = int(high_susp_ind[0][0])
	# for n in high_src_ind:
	# 	print(n)
	# print(arr[high_src_ind])
	len_src = int(high_src_off) - int(low_src_off) + int(arr[high_src_ind][13])
	len_susp = int(high_susp_off) - int(low_susp_off) + int(arr[high_susp_ind][14])

	susp_off_gap = gap(group, 12, 14)
	src_off_gap = gap(group, 11, 13)

	plagType = "NoPlagiarism"
	for g in group:
		if "NoPlagiarism" not in g[9]:
			plagType = g[9]

	# print("---")
	# # print(group)
	# print([low_src_off,low_susp_off,len_src,len_susp,susp_off_gap])
	# if susp_off_gap < 0:
	# 	print(offs)
	# 	print(susp_off_gap)
	# if(low_susp_off == 8529):
	# 	print("HERE I AM")
	# 	print("HERE I AM")
	# 	print("HERE I AM")
	# 	print("HERE I AM")
	# 	print(len(group))
	# print((low_src_off,low_susp_off,len_src,len_susp,susp_off_gap,src_off_gap))
	return (low_src_off,low_susp_off,len_src,len_susp,susp_off_gap,src_off_gap,plagType)

def string_pair(pairName, dataPath):
	r = re.match(r'.*(suspicious-document[0-9]{5}).*',pairName,re.M|re.I)
	suspName = r.group(1)
	r = re.match(r'.*(source-document[0-9]{5}).*',pairName,re.M|re.I)
	srcName = r.group(1)
	srcDoc = os.path.join(dataPath+"/src/", srcName+".txt")
	suspDoc = os.path.join(dataPath+"/susp/", suspName+".txt")
	src_fp = codecs.open(srcDoc, 'r', 'utf-8')
	src_text = src_fp.read()
	susp_fp = codecs.open(suspDoc, 'r', 'utf-8')
	susp_text = susp_fp.read()

	return(src_text,susp_text,srcName,suspName)

def is_plag(panDir, metrics):
	#metrics: srcDoc","suspDoc","src_off","susp_off","src_len","susp_len","cos","dice"
	# print(metrics)
	paths = [panDir+"\\02-no-obfuscation",panDir+"\\03-random-obfuscation",panDir+"\\04-translation-obfuscation",panDir+"\\05-summary-obfuscation"]
	# print(paths)
	impl = xml.dom.minidom.getDOMImplementation()

	for path in paths:
		xml_file = "{}\\{}-{}.xml".format(path,metrics[1],metrics[0])

		if(not os.path.isfile(xml_file)):
			continue
		doc = xml.dom.minidom.parse(xml_file)
		# print("Got doc!")

		features = doc.getElementsByTagName("feature")
		for f in features:
			if(f.getAttribute("name") == "plagiarism"):
				plag_src_off = int(f.getAttribute("source_offset"))
				plag_src_len = int(f.getAttribute("source_length"))
				plag_susp_off = int(f.getAttribute("this_offset"))
				plag_susp_len = int(f.getAttribute("this_length"))
				obfuscation = f.getAttribute("obfuscation")
				if(obfuscation == "" or obfuscation == None):
					obfuscation = f.getAttribute("type")
				if len(obfuscation) <= 1:
					obfuscation = "NoPlagiarism_Summary"

				src_plag = set(range(plag_src_off,plag_src_off+plag_src_len))
				susp_plag = set(range(plag_susp_off,plag_susp_off+plag_susp_len))
				src_pass = range(metrics[2],metrics[2]+metrics[4])
				susp_pass = range(metrics[3],metrics[3]+metrics[5])
				intersect_src = src_plag.intersection(src_pass)
				intersect_susp = susp_plag.intersection(susp_pass)


				if intersect_src and intersect_susp:
					# if "0021" in metrics[0] and "2280" in metrics[1]:
					# 	print("-------")
					# 	print(min(intersect_src))
					# 	print(max(intersect_src))
					# 	print(plag_src_off)
					# 	print(plag_src_off + plag_src_len)
					# print(len(intersect_susp)/len(susp_pass))

					return "True",(len(intersect_susp)*len(intersect_src))/(len(susp_pass)*len(src_pass))

	return ("False",0.0)

def passage_similarity(tokens, passages):

	#tokens
	src_text = tokens[0]
	susp_text = tokens[1]
	#passage conversion
	#(src_off, susp_off, src_len, susp_len)
	src_offsets = []
	susp_offsets = []
	# print(passages)
	# for p in passages:
	# 	src_offsets.append([ p[0],p[2] ])
	# 	susp_offsets.append([ p[1],p[3] ])

	src_voc = {}
	src_sents = []
	src_bow=tokenize(src_text,src_voc,src_offsets,src_sents, "no")
	# ss_treat(src_bow,src_offsets,3,"no")

	susp_voc = {}
	susp_sents = []
	susp_bow=tokenize(susp_text,susp_voc,susp_offsets,susp_sents, "no")
	# ss_treat(susp_bow,susp_offsets,3,"no")
	pass_susp_bow = {}
	pass_src_bow = {}
	# print(len(susp_bow))
	# print(len(susp_offsets))
	plag = []
	for p in passages:
		p_su_o = int(p[1])
		p_su_l = int(p[3])
		p_sr_o = int(p[0])
		p_sr_l = int(p[2])
		susp_gap = int(p[4])
		src_gap = int(p[5])
		plag_type = p[6]
		pass_susp_bow[p_su_o] = []
		pass_src_bow[p_sr_o] = []

		for i,o in enumerate(susp_offsets):
			if int(o[0]) in range(int(p_su_o), int(p_su_o)+int(p_su_l)):
				# print(pass_susp_bow[p_su_o])
				# print(susp_bow[i])
				pass_susp_bow[p_su_o] = sum_vect(dict(pass_susp_bow[p_su_o]),susp_bow[i])
		if(len(pass_susp_bow[p_su_o]) == 0):
			print("Empty BOW SUSP")


		for i,o in enumerate(src_offsets):
			# print(o[0])
			if int(o[0]) in range(int(p_sr_o), int(p_sr_o)+int(p_sr_l)):
				pass_src_bow[p_sr_o] = sum_vect(dict(pass_src_bow[p_sr_o]),src_bow[i])
		if(len(pass_src_bow[p_sr_o]) == 0):
			# print("----")
			# print(p_sr_o)
			# print(int(p_sr_l))

			print("Empty BOW SRC")


		if len(pass_src_bow[p_sr_o]) == 0 or len(pass_susp_bow[p_su_o] ) == 0:
			print("What the...")
			continue
		cos = cosine_measure(pass_src_bow[p_sr_o],pass_susp_bow[p_su_o])
		dice = dice_coeff(pass_src_bow[p_sr_o],pass_susp_bow[p_su_o])

		plag.append([p_sr_o,p_su_o,p_sr_l,p_su_l,cos,dice,susp_gap,src_gap,plag_type])

	return plag

if len(sys.argv) == 4:
	# t1=time.time()
	input_file = sys.argv[1]
	output_file = sys.argv[2]
	pan_path = sys.argv[3]
else:
	print(len(sys.argv))
	print("Expecting arguments: input_file output_file pan_path")
	exit()
with open(input_file, 'r') as csvfile:

	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	# ['"Pair"', '"SrcSenOff"', '"SuspSenOff"',
	# 3 '"PlagType"', '"Cos"', '"IsMaxCos"', '"NeighbourMaxCos"', '"Dice"',
	# 8 '"IsMaxDice"', '"NeighbourMaxDice"', '"PotentialPlagType"', '"LikelyPlag;"',
	# 12 "SrcOff", "SuspOff", "SrcLen", "SuspLen"]
	# 16 '"IsPlag"', '"Prediction (LikelyPlag;)"',

	pairsName = ""
	pairCat = []
	pairs = {}
	numPairs = 0
	with open(output_file, 'a') as f:
		writer = csv.writer(f, lineterminator='\n')
		f.write("{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("srcDoc","suspDoc","src_off","susp_off","src_len","susp_len","cos","dice","susp_gap","src_gap","PlagType","PlagAcc","IsPlag"))
		i = 0
		j= 0
		for row in csvr:
			# print("{} {} {}".format(row[12],row[13],row[23]))
			if "Pair" in row[0]:
				continue
			if pairsName == "":
				# print("init")
				pairsName = row[0] # Initialize
				pairs = []

			if row[0] != pairsName:

				if "Pair" in pairsName:
					continue

				print(pairsName)
				if pairsName not in pairCat:
					pairCat.append(pairsName)
					numPairs +=1
				else:
					print("OLD")


				seeds = {}
				pairs.sort(key=lambda x: (int(x[1]),int(x[0])))  #Sort by susp
				# print("PAIRS")
				# print(pairs)

				# for r in pairs: # Fill seeds[susp][src]
				# 	# print(r)
				# 	if "False" in str(r[22]):
				# 		# print(r)
				# 		continue
				# 	if int(r[1]) not in seeds:
				# 		seeds[int(r[1])] = {}
				# 		seeds[int(r[1])][int(r[0])] = r
				# 	elif int(r[0]) not in seeds[int(r[1])]:
				# 		seeds[int(r[1])][int(r[0])] = r
				# 	else:
				# 		print("ALREADY THERE")

				groups = []
				currentGroup = []

				for r in pairs:
					if len(currentGroup) == 0: #INITwinwin
						currentGroup.append(r)
					else:
						of = r[1]
						dist = int(of) - int(currentGroup[-1][1])
						if dist >= 0 and dist <= 1: #Adjacent, append
							currentGroup.append(r)
							currentGroup.sort(key=lambda x: int(x[1]))
						else: #Out of range
							groups.append(currentGroup)
							currentGroup = []
				if len(currentGroup) != 0:
					groups.append(currentGroup)

				# for g in groups:
				# 	print("NG")
				# 	for s in g:
				# 		print(s[0:2])
				if len(groups) >= 1:
					groups = permute(groups,12)
					print(len(groups))

					pairTokens = string_pair(pairsName, pan_path)
					passages = []

					# if len(groups) > 1:
					# 	print(groups)
					for g in groups:
						passages.append(to_passage(g)) #(src_off, susp_off, src_len, susp_len,gap)
					i+=len(passages)

					# for p in passages:
					# 	if(p[0] == 0):
					# 		print(p)

					metrics = passage_similarity(pairTokens,passages)  #(p_sr_o,p_su_o,p_sr_l,p_su_l,cos,dice))
					j+=len(metrics)
					# print(len(metrics))
					#"srcDoc","suspDoc","src_off","susp_off","src_len","susp_len","cos","dice","susp_gap","src_gap,"plagAcc", "IsPlag"
					for o in metrics:
					# out = metrics
						# if(o[0] == 0):
							# print(o[0:4])
						o.insert(0, pairTokens[2])
						o.insert(1, pairTokens[3])
						isPlag = is_plag(pan_path,o)
						# print(isPlag)
						o.append(isPlag[1])
						o.append(isPlag[0])
						# print(o)
						# if("False" in o[-1] ):
						# print(o)
						# print("row")
						writer.writerow(o)
					# print(len(groups))
				else:
					print("NO OUT")
					print(groups)
					print(currentGroup)
					print(seeds)
					print(pairs)


				# Reset
				pairs = []
				pairsName = row[0]
				pairs.append(row[1:])
			else:
				# print("add")
				pairs.append(row[1:])

	print(i)
	print(j)
	print("Numpairs: "+str(numPairs))
	# print(pairCat)