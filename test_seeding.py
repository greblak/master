import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import xml.dom.minidom

with open("./out_test/seeding_predictions_test.csv" , 'r') as csvfile:
	plag_check = {}
	plag_src_count = 0
	plag_susp_count = 0
	seeds = csv.reader(csvfile, delimiter=',', quotechar='|')

	badCounts = 0
	badSeeds = []
	n = 0

	for s in seeds:

		if "Pair" in s[0]:
			continue

		r = re.match(r'.*(suspicious-document[0-9]{5}).*',s[0],re.M|re.I)
		suspName = r.group(1)
		r = re.match(r'.*(source-document[0-9]{5}).*',s[0],re.M|re.I)
		srcName = r.group(1)

		panDir = "pan_test"
		plags = []
		paths = [panDir+"\\02-no-obfuscation",panDir+"\\03-random-obfuscation",panDir+"\\04-translation-obfuscation",panDir+"\\05-summary-obfuscation"]
		impl = xml.dom.minidom.getDOMImplementation()
		for path in paths:
			xml_file = "{}\\{}-{}.xml".format(path,suspName,srcName)

			if(not os.path.isfile(xml_file)):
				continue
			doc = xml.dom.minidom.parse(xml_file)
			# print("Got doc!")

			features = doc.getElementsByTagName("feature")
			for f in features:
				if(f.getAttribute("name") == "plagiarism"):
					src_off = f.getAttribute("source_offset")
					src_len = f.getAttribute("source_length")
					susp_off = f.getAttribute("this_offset")
					susp_len = f.getAttribute("this_length")
					obfuscation = f.getAttribute("obfuscation")
					if(obfuscation == "" or obfuscation == None):
						obfuscation = f.getAttribute("type")
					if len(obfuscation) <= 1:
						obfuscation = "NoPlagiarism_Summary"
					plags.append((src_off, src_len, susp_off, susp_len, obfuscation))
		# print(s[0])
		if("Pair" in s[0]):
			# print(s[12:16])
			continue

		n+=1
		if n % 1000 == 0:
			print(n)
		within_src = False
		within_susp = False

		se = s[10:15]
		se_src = range(int(se[0]), int(se[0])+int(se[2]))
		se_susp = range(int(se[1]), int(se[1])+int(se[3]))

		# m = 0
		for p in plags:
			ps = "{} - {} - {}".format(s[0],p[0],p[2])
			if ps not in plag_check:
				plag_check[ps] = 0
				# plag_src_count += 1
				# plag_susp_count += 1
			# print("Testing ")
			# m+=1
			# print(srcName)
			# print(suspName)

			pl = p
			pl_src = range(int(pl[0]), int(pl[0])+int(pl[1]))
			pl_susp = range(int(pl[2]), int(pl[2])+int(pl[3]))
			if set(se_src).intersection(pl_src):
				# print(s[0]+"sr"+se[0]+" " +pl[0] + " " +str(int(pl[0])+int(pl[2])))
				within_src = True
				plag_check[ps] += 1
				plag_src_count -= 1
				if len(set(se_src).intersection(pl_src)) != len(se_src):
					print(len(set(se_src).intersection(pl_src)) / len(se_src))
			if set(se_susp).intersection(pl_susp):
				# print(s[0]+"su")
				within_susp = True
				plag_check[ps] += 1
				plag_susp_count -= 1



		if within_src is False or within_susp is False:
			badCounts += 1
			badSeeds.append(s)
		# print(m)
	print("-----")
	print(badCounts)
	print(n)
	print(str(badCounts / float(n)))
	print("-----")
	print(plag_src_count)
	print(plag_susp_count)
	bad_plag = 0
	good_plag = 0
	for b in plag_check:
		if plag_check[b] > 1:
			print(plag_check[b])
	# print(len(plag_check))
	print(bad_plag)
	# print(good_plag)
	for s in badSeeds:
		print(s[0])
			# print(badSeeds)