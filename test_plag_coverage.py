import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy
import xml.dom.minidom


panDir = "pan_test"
testDir = "out_test/xml"
paths = [panDir+"\\02-no-obfuscation",panDir+"\\03-random-obfuscation",panDir+"\\04-translation-obfuscation",panDir+"\\05-summary-obfuscation"]
impl = xml.dom.minidom.getDOMImplementation()
rats = []
for path in paths:
	files = os.listdir(path)
	for fi in files:
		if "xml" not in fi: # Skip pair files
			continue

		print(fi)
		doc = xml.dom.minidom.parse(path+"/"+fi)
		# print("Got doc!")

		pl_su = []
		pl_sr = []

		first_pla = 0
		pla = {}


		features = doc.getElementsByTagName("feature")
		for f in features:
			if(f.getAttribute("name") == "plagiarism"):
				src_off = int(f.getAttribute("source_offset"))
				src_len = int(f.getAttribute("source_length"))
				susp_off = int(f.getAttribute("this_offset"))
				susp_len = int(f.getAttribute("this_length"))
				first_pla = susp_off
				# obfuscation = f.getAttribute("obfuscation")
				# if(obfuscation == "" or obfuscation == None):
					# obfuscation = f.getAttribute("type")
				# if len(obfuscation) <= 1:
					# obfuscation = "NoPlagiarism_Summary"
				for n in range(susp_off, susp_off+susp_len):
					pla[n] = list(range(src_off, src_off+src_len))
		if len(features) < 1 or len(pla) < 1:
			continue

		num_pla = len(pla) * len(pla[susp_off])
		num_cov = 0
		if num_pla < 1:
			continue
		subPaths = os.listdir(testDir)

		dec = {}

		for pa in subPaths:
			fil = os.listdir(testDir+"/"+pa)
			for fii in fil:
				if "xml" not in fii: # Skip pair file
					continue


				if fi not in fii:
					continue


				doc2 = xml.dom.minidom.parse(testDir+"/"+pa+"/"+fii)
				feats = doc2.getElementsByTagName("feature")
				for f in feats:
					if(f.getAttribute("name") == "detected-plagiarism"):
						# print("Feat")
						src_off = int(f.getAttribute("source_offset"))
						src_len = int(f.getAttribute("source_length"))
						susp_off = int(f.getAttribute("this_offset"))
						susp_len = int(f.getAttribute("this_length"))
						# obfuscation = f.getAttribute("obfuscation")
						# if(obfuscation == "" or obfuscation == None):
							# obfuscation = f.getAttribute("type")
						# if len(obfuscation) <= 1:
							# obfuscation = "NoPlagiarism_Summary"
						for n in range(susp_off, susp_off+susp_len):
							dec[n] = list(range(src_off, src_off+src_len))
				if len(feats) < 1 or len(dec) < 1:
					continue
		rat = 0

		ints = 0
		for su in pla:
			if su not in dec:
				continue
			for sr in pla[su]:
				if sr in dec[su]:
					ints+=1
		rat = ints/(len(pla)*len(pla[first_pla]))
		print(rat)
		rats.append(rat)
	print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
	print(path)
	subAvg = sum(rats)/len(rats)
	rats = []
	print(subAvg)
	print("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
