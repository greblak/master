import collections
import nltk
import os
import sys
import codecs
import re
import math
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy

with open("./out_test/seeding_predictions_test.csv" , 'r') as csvfile:
	seeds = csv.reader(csvfile, delimiter=',', quotechar='|')

	badCounts = 0
	badSeeds = []
	n = 0
	for s in seeds:
		with open("out_test/to_xml.csv ", 'r') as csvfile2:
			plags = csv.reader(csvfile2, delimiter=',', quotechar='|')
			# print(s[0])
			if("Pair" in s[0]):
				# print(s[12:16])
				continue

			n+=1
			if n % 1000 == 0:
				print(n)
			within_src = False
			within_susp = False

			se = s[12:16]
			se_src = range(int(se[0]), int(se[0])+int(se[2]))
			se_susp = range(int(se[1]), int(se[1])+int(se[3]))

			r = re.match(r'.*(suspicious-document[0-9]{5}).*',s[0],re.M|re.I)
			suspName = r.group(1)
			r = re.match(r'.*(source-document[0-9]{5}).*',s[0],re.M|re.I)
			srcName = r.group(1)

			# m = 0
			for p in plags:
				if "srcDoc" in p[0]:
					# print(p[2:6])
					continue

				if p[0] not in srcName or p[1] not in suspName:
					continue

				# print("Testing ")
				# m+=1
				# print(srcName)
				# print(suspName)

				pl = p[2:6]
				pl_src = range(int(pl[0]), int(pl[0])+int(pl[2]))
				pl_susp = range(int(pl[1]), int(pl[1])+int(pl[3]))
				if set(se_src).intersection(pl_src):
					# print(s[0]+"sr"+se[0]+" " +pl[0] + " " +str(int(pl[0])+int(pl[2])))
					within_src = True
				if set(se_susp).intersection(pl_susp):
					# print(s[0]+"su")
					within_susp = True

			if within_src is False or within_susp is False:
				badCounts += 1
				badSeeds.append(s)
		# print(m)
	print("-----")
	print(badCounts)
	print(n)
	print(str(badCounts / float(n)))
	for s in badSeeds:
		print(s[0])
			# print(badSeeds)