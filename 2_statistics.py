import math
import statistics
import time
import codecs
import re
import sys
import csv
import matplotlib.pyplot as plt
import numpy as numpy

if len(sys.argv) != 3:
	print("Expecting arguments: PanDir OutDir")
	exit()

def prepData(rows):
	pair = {}
	for row in rows:
		# print(row)
		suspSenNum = row[0]
		srcSenNum = row[1]
		if srcSenNum not in pair or len(pair[srcSenNum]) is 0:
			pair[srcSenNum] = {}
		pair[srcSenNum][suspSenNum] = row[2:]
		# print("{} - {} ".format(srcSenNum,suspSenNum))
	return pair

def calcStats(pairs, pairName,file):

	print(pairName)
	cos_heatmap = []
	dice_heatmap = []
	plagmap = []
	plagTypes = []
	cos_maxMap = []
	dice_maxMap = []

	# print(len(pairs))
	# print(len(pairs[0]))

	#Create basic pairing/cos_heatmap
	for srcSen in pairs:
		cos_heatmap.append([])
		dice_heatmap.append([])
		plagmap.append([])
		cos_maxMap.append([0.]*len(pairs[srcSen]))
		# print("Appended {} columns".format(len(pairs[srcSen])))
		dice_maxMap.append([0.]*len(pairs[srcSen]))
		for suspSen in pairs[srcSen]:
			cos_heatmap[srcSen].append(pairs[srcSen][suspSen][0])
			dice_heatmap[srcSen].append(pairs[srcSen][suspSen][6])
			if  "NoPlagiarism" not in pairs[srcSen][suspSen][1]:
				plagmap[srcSen].append(1.)
				# print("Score!")
			else:
				plagmap[srcSen].append(0.)
			if pairs[srcSen][suspSen][1] not in plagTypes:
				plagTypes.append(pairs[srcSen][suspSen][1] )

	#Calculate max
	neigh_max_cos = []
	neigh_max_dice = []
	for susp in range(0,len(cos_heatmap[0])):
		neigh_max_cos.append(0.0)
		m_cos = max(numpy.array(cos_heatmap)[:,susp])

		sources = numpy.where(numpy.matrix(cos_heatmap)[:,susp] == m_cos)
		for src in sources:
			try:
				if m_cos > 0.0:
					cos_maxMap[src][susp] = m_cos
					neigh_max_cos[susp] = m_cos
			except TypeError:
				pass
	for susp in range(0,len(dice_heatmap[0])):
		neigh_max_dice.append(0.0)
		m_dice = max(numpy.array(dice_heatmap)[:,susp])

		sources = numpy.where(numpy.array(dice_heatmap)[:,susp] == m_dice)
		for src in sources:
			try:
				if m_dice > 0.0:
					dice_maxMap[src][susp] = m_dice
					neigh_max_dice[susp] = m_dice
			except TypeError:
				pass

	#Write to file
	# print(len(pairs))
	# print(neigh_max_dice)

	meanCos = statistics.mean([float(i) for i in neigh_max_cos])
	meanDice = statistics.mean([float(i) for i in neigh_max_dice])

	for src in pairs:
		# print(len(pairs[src]))
		for susp in pairs[src]:
			neighbour_max_cos = 0.
			neighbour_max_dice = 0.
			if susp is 0 and len(neigh_max_cos) > 1:
				# print(susp)
				# print(len(pairs[src]))
				# print(len(cos_maxMap[src]))
				# print(susp)
				# print(neigh_max_cos)
				neighbour_max_cos = neigh_max_cos[susp+1]
				neighbour_max_dice = neigh_max_dice[susp+1]
			elif susp >= len(pairs[src])-1:
				neighbour_max_cos = neigh_max_cos[susp-1]
				neighbour_max_dice = neigh_max_dice[susp-1]
			else:
				neighbour_max_cos = max(neigh_max_cos[susp+1], neigh_max_cos[susp-1])
				neighbour_max_dice = max(neigh_max_dice[susp+1], neigh_max_dice[susp-1])



				# print(src)
			pair = pairs[src][susp]
			isMaxCos = False
			isMaxDice = False
			# Pair format: [cos,plagType,src_sen, susp_sen,src_offset]
			srcDoc = pair[11]
			suspDoc = pair[12]
			cos = pair[0]
			dice = pair[6]
			plagType = pair[1]
			src_sen = pair[2]
			susp_sen = pair[3]
			potentialPlagType = pair[5]
			src_offset = pair[7]
			susp_offset = pair[8]
			src_length = pair[9]
			susp_length = pair[10]
			likelyPlag = False
			if cos >= cos_maxMap[src][susp] and cos_maxMap[src][susp] > 0.0:
				isMaxCos = True
			if dice >= dice_maxMap[src][susp] and dice_maxMap[src][susp] > 0.0:
				isMaxDice = True


			cosMax = numpy.where(numpy.matrix(cos_maxMap)[:,susp] > 0.0)[0]
			diceMax = numpy.where(numpy.matrix(dice_maxMap)[:,susp] > 0.0)[0]
			# print(cos_maxMap)

			vertDistCos = sys.maxsize
			vertDistDice = sys.maxsize

			for s in cosMax:
				if len(s.tolist())>0:
					if len(s.tolist()[0]) > 1:
						cosMaxPos = s.tolist()[0][1]
						vertDistCos = math.fabs(cosMaxPos-src)
					elif len(s.tolist()[0]) == 1:
						cosMaxPos = s.tolist()[0][0]
						vertDistCos = math.fabs(cosMaxPos-src)

			for s in diceMax:
				if(len(s.tolist())>0) and len(s.tolist()[0])>0:
						diceMaxPos = s.tolist()[0][0]
						vertDistDice = math.fabs(diceMaxPos-src)

			distFromMaxCos = neigh_max_cos[susp] - cos
			distFromMaxDice = neigh_max_dice[susp] - dice
			distFromMeanDice = meanDice - dice
			distFromMeanCos = meanCos - cos

			if "NoPlagiarism" not in potentialPlagType and (isMaxCos or isMaxDice or (neigh_max_cos[susp] > 0.0 and (1.0*cos)/neigh_max_cos[susp] >0.4) or (neigh_max_dice[susp] > 0.0 and (1.0*dice)/neigh_max_dice[susp] >0.4)):
				likelyPlag = True

			pairName = "Susp: {} - Src:{}".format(suspDoc,srcDoc)
			r = re.match(r'.*(suspicious-document[0-9]{5}).*',suspDoc,re.M|re.I)
			suspName = r.group(1)
			r = re.match(r'.*(source-document[0-9]{5}).*',srcDoc,re.M|re.I)
			srcName = r.group(1)


			# if cos > 0.95 and src_length == susp_length:
			# 	srcFile = codecs.open(sys.argv[1]+"/src/{}.txt".format(srcName), 'r', 'utf-8').read()
			# 	suspFile = codecs.open(sys.argv[1]+"/susp/{}.txt".format(suspName), 'r', 'utf-8').read()
			# 	suspQuote=suspFile
			# 	srcQuote=srcFile
			# 	print("----------------------")
			# 	print(pairName)
			# 	print("{} {} - {} {}".format(src_offset, src_length, susp_offset,susp_length))
			# 	print("SUSP: {}".format(suspQuote[int(susp_offset):int(susp_offset)+int(susp_length)]).replace('\n', ''))
			# 	print("SRC: {}".format(  srcQuote[int(src_offset): int(src_offset) +int(src_length)]).replace('\n', ''))

			# f.write("{},{},{},{},{},{},{},{},{},{},{},{},{};\n".format("Pair", SrcSen", "SuspSen","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag"))
			file.write("|{}|,|{}|,|{}|,{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format(pairName, src,susp,plagType,cos, isMaxCos, neighbour_max_cos,dice, isMaxDice, neighbour_max_dice, potentialPlagType, likelyPlag,src_offset,susp_offset,src_length, susp_length,distFromMaxCos,distFromMaxDice,distFromMeanCos,distFromMeanDice,vertDistCos,vertDistDice))

	# print("Complete!")


with open(sys.argv[2]+'/cosData.csv', 'r') as csvfile:
	#print(data)
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	rowNum = 0
	# numTotal = 19700339 # From doing line below on complete data
	# print("Calculating total...")
	# numTotal = sum(1 for row in csvr)
	# csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	# print("{} lines".format(numTotal))
	# data = {}
	# pair = {}
	# i = 0

	with open(sys.argv[2]+"/stat.csv", 'a') as f:
		# Write header to output file.
		f.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen","distFromMaxCos","distFromMaxDice","distFromMeanCos","distFromMeanDice","vertDistCos","vertDistDice"))

		currentPairData = []
		currentPairName = ""
		i = 0
		t = time.time()
		start_time = time.time()
		for row in csvr:
			# Format of input file
			# g.write("{},{},{},{},{},{},{},|{}|,|{}|,{}\n".format(self.srcDoc, self.suspDoc, c,r, cos_mes, potentialPlagType.encode(sys.stdout.encoding,errors='replace'),plagType.encode(sys.stdout.encoding, errors='replace'), src_sent.encode(sys.stdout.encoding, errors='replace'), susp_sent.encode(sys.stdout.encoding, errors='replace'),self.src_offsets[r][0]))
			src = row[0]
			susp = row[1]

			suspSenNum = int(row[2])
			srcSenNum = int(row[3])
			cos = float(row[4])
			dice = float(row[5])
			potentialPlagType = row[6]
			plagType = row[7]
			src_sen = row[8]
			susp_sen = row[9]
			src_offset = row[10]
			susp_offset = row[11]
			src_length = row[12]
			susp_length = row[13]
			pairName = "Susp: {} - Src:{}".format(susp,src).replace('\n', '')

			currentPairRow = [suspSenNum, srcSenNum, cos,plagType,src_sen, susp_sen,src_offset, potentialPlagType, dice,src_offset,susp_offset,src_length, susp_length,src,susp]

			# print(currentPairName)
			# print(pairName)
			if len(currentPairData) is 0: # Should only run on very first row to initialize
				# print("INIT!")
				currentPairData.append(currentPairRow)
				currentPairName = pairName
			elif pairName == currentPairName:
				# print("Append")
				currentPairData.append(currentPairRow)
			else: #New pair, calculate old and reset
				#Calculate and write output
				# print("New")
				i+=1
				# if "00635" in susp and "00265" in src:
				prepDat = prepData(currentPairData)
				calcStats(prepDat, pairName,f)
				print(i)
				#Reset and add new pair
				currentPairData = []
				currentPairName = pairName
				currentPairData.append(currentPairRow)
		if len(currentPairData) > 0:
			i+=1
			prepDat = prepData(currentPairData)
			calcStats(prepDat, pairName,f)
		print(i)
