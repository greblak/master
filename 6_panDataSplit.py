import sys
import os
import random
import csv
import shutil
import re
import os.path

def copyFilePair(pair):
	r = re.match(r'.*(suspicious-document[0-9]{5}).*',pair,re.M|re.I)
	suspName = r.group(1)
	r = re.match(r'.*(source-document[0-9]{5}).*',pair,re.M|re.I)
	srcName = r.group(1)
	fileStr = "{}-{}.xml".format(suspName, srcName)

	for i in range(1,6):
		if not os.path.exists(sys.argv[2]+"/panSet/0{}".format(i)):
			os.makedirs(sys.argv[2]+"/panSet/0{}".format(i))

	if os.path.isfile(sys.argv[1]+"/01-no-plagiarism/{}".format(fileStr)) :
		print("DING 1")
		shutil.copyfile(sys.argv[1]+"/01-no-plagiarism/{}".format(fileStr), sys.argv[2]+"/panSet/01/{}".format(fileStr))
		with open(sys.argv[2]+'/panSet/01/pairs', 'a') as p:
			p.write("{} {}".format(suspName,srcName))
	elif os.path.isfile(sys.argv[1]+"/02-no-obfuscation/{}".format(fileStr)) :
		print("DING 2")
		shutil.copyfile(sys.argv[1]+"/02-no-obfuscation/{}".format(fileStr), sys.argv[2]+"/panSet/02/{}".format(fileStr))
		with open(sys.argv[2]+'/panSet/02/pairs', 'a') as p:
			p.write("{} {}".format(suspName,srcName))
	elif os.path.isfile(sys.argv[1]+"/03-random-obfuscation/{}".format(fileStr)) :
		print("DING 3 ")
		shutil.copyfile(sys.argv[1]+"/03-random-obfuscation/{}".format(fileStr), sys.argv[2]+"/panSet/03/{}".format(fileStr))
		with open(sys.argv[2]+'/panSet/03/pairs', 'a') as p:
			p.write("{} {}".format(suspName,srcName))
	elif os.path.isfile(sys.argv[1]+"/04-translation-obfuscation/{}".format(fileStr)) :
		print("DING 4")
		shutil.copyfile(sys.argv[1]+"/04-translation-obfuscation/{}".format(fileStr), sys.argv[2]+"/panSet/04/{}".format(fileStr))
		with open(sys.argv[2]+'/panSet/04/pairs', 'a') as p:
			p.write("{} {}".format(suspName,srcName))
	elif os.path.isfile(sys.argv[1]+"/05-summary-obfuscation/{}".format(fileStr)) :
		print("DING 5")
		shutil.copyfile(sys.argv[1]+"/05-summary-obfuscation/{}".format(fileStr), sys.argv[2]+"/panSet/05/{}".format(fileStr))
		with open(sys.argv[2]+'/panSet/05/pairs', 'a') as p:
			p.write("{} {}".format(suspName,srcName))
	else:
		print(sys.argv[1])
		print(fileStr)




# random.seed(42) # You know why....	
with open(sys.argv[2]+'/seeding_predictions_test.csv', 'r') as csvfile:
	#Generate pair list
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	pairs = []
	for row in csvr:
		pair = row[0]
		if "Pair" in pair:
			continue
		if pair not in pairs:
			pairs.append(pair)
			print(pair)
			copyFilePair(pair)
			

	#Copy relevant files
	