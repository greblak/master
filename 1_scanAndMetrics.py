import os
import sys
import xml.dom.minidom
import codecs
import nltk
import re
import math
import time
import flist


def jaccard(a, b):
	c = set(a.keys()) & set(b.keys())
	return float(len(c)) / (len(a) + len(b) - len(c))

# From Sanchez-Perez
def tf_idf(list_dic1,voc1,list_dic2,voc2):
	idf=sum_vect(voc1,voc2)
	td=len(list_dic1)+len(list_dic2)
	for i in range(len(list_dic1)):
		for j in list(list_dic1[i].keys()):
			list_dic1[i][j]*=math.log(td/float(idf[j]))
	for i in range(len(list_dic2)):
		for j in list(list_dic2[i].keys()):
			list_dic2[i][j]*=math.log(td/float(idf[j]))

# From Sanchez-Perez
def eucl_norm(d1):
		norm=0.0
		for val in list(d1.values()):
			norm+=float(val*val)
		return math.sqrt(norm)

# From Sanchez-Perez
def cosine_measure(d1,d2):
	dot_prod=0.0
	det=eucl_norm(d1)*eucl_norm(d2)
	if det==0:
		return 0
	for word in list(d1.keys()):
		if word in d2:
			dot_prod+=d1[word]*d2[word]
	return dot_prod/det

# From Sanchez-Perez
def dice_coeff(d1,d2):
	if len(d1)+len(d2)==0:
		return 0
	intj=0
	for i in list(d1.keys()):
		if i in d2:
			intj+=1
	return 2*intj/float(len(d1)+len(d2))


# From Sanchez-Perez
def sum_vect(dic1,dic2):
	res=dic1
	for i in list(dic2.keys()):
		if i in res:
			res[i]+=dic2[i]
		else:
			res[i]=dic2[i]
	return res

# From Sanchez-Perez
def ss_treat(list_dic,offsets,min_sentlen,rssent):
	if rssent=='no':
		i=0
		range_i=len(list_dic)-1
		while i<range_i:
			if sum(list_dic[i].values())<min_sentlen:
				list_dic[i+1]=sum_vect(list_dic[i+1],list_dic[i])
				del list_dic[i]
				offsets[i+1]=(offsets[i][0],offsets[i+1][1]+offsets[i][1])
				del offsets[i]
				range_i-=1
			else:
				i=i+1
	else:
		i=0
		range_i=len(list_dic)-1
		while i<range_i:
			if sum(list_dic[i].values())<min_sentlen:
				del list_dic[i]
				del offsets[i]
				range_i-=1
			else:
				i=i+1

def is_overlap(off_a, len_a, off_b, len_b):
	if off_a >= off_b and off_a <= off_b + len_b:
		return True
	if off_a+len_a > off_b and off_a+len_a <= off_b + len_b:
		return True
	if off_a < off_b and off_a + len_a > off_b + len_b:
		return True
	return False



# From Sanchez-Perez
def tokenize(text,voc={},offsets=[],sents=[],rem_sw='no'):
	"""
	INPUT:  text: Text to be pre-processed
		   voc: vocabulary used in the text with idf
			offsets: start index and length of each sentence
			sents: sentences of the text without tokenization
	OUTPUT: Returns a list of lists representing each sentence divided in tokens
	"""
	#text.replace('\0x0', ' ')
	sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')
	sents.extend(sent_detector.tokenize(text))
	offsets.extend([(a,b-a) for (a,b) in sent_detector.span_tokenize(text)])
	sent_tokens=[nltk.TreebankWordTokenizer().tokenize(sent) for sent in sents]
	stemmer=nltk.stem.porter.PorterStemmer()
	sent_tokens_pp=[]
	stopwords=flist.flist().words()
	cont=0
	for tokens in sent_tokens:
		if rem_sw=='no':
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word)]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		elif rem_sw=='50':
			stopwords=flist.flist().words50()
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word) and word.lower() not in stopwords]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		else:
			temp={}
			for i in [stemmer.stem(word.lower()) for word in tokens if re.match(r'([a-zA-Z]|[0-9])(.)*',word) and word.lower() not in stopwords]:
				if i in temp:
					temp[i]+=1
				else:
					temp[i]=1
		if len(temp)>0:
			sent_tokens_pp.append(temp)
			for i in list(temp.keys()):
				if i in voc:
					voc[i]+=1
				else:
					voc[i]=1
			cont=cont+1
		else:
			del offsets[cont]
	return sent_tokens_pp

###
# Main detector
# takes source dir, susp dir, output dir
###
class plagdet:
	def preprocess(self):
		#bag of words generation and sentence splitting
		self.src_bow=tokenize(self.src_text,self.src_voc,self.src_offsets,self.src_sents,self.rem_sw)
		ss_treat(self.src_bow,self.src_offsets,self.min_sentlen,self.rssent)
		# print(self.src_bow)

		self.susp_bow=tokenize(self.susp_text,self.susp_voc,self.susp_offsets,self.susp_sents,self.rem_sw)
		ss_treat(self.susp_bow,self.susp_offsets,self.min_sentlen,self.rssent)

		self.plag_passages = []

		tf_idf(self.src_bow,self.src_voc,self.susp_bow,self.susp_voc)

		# print(str(self.src_voc).encode(sys.stdout.encoding, errors='replace'))

		paths = [self.panDir+"\\02-no-obfuscation",self.panDir+"\\03-random-obfuscation",self.panDir+"\\04-translation-obfuscation",self.panDir+"\\05-summary-obfuscation"]
		impl = xml.dom.minidom.getDOMImplementation()
		for path in paths:
			xml_file = "{}\\{}-{}.xml".format(path,self.susp[:-4],self.src[:-4])

			if(not os.path.isfile(xml_file)):
				continue
			doc = xml.dom.minidom.parse(xml_file)
			# print("Got doc!")

			features = doc.getElementsByTagName("feature")
			for f in features:
				if(f.getAttribute("name") == "plagiarism"):
					src_off = f.getAttribute("source_offset")
					src_len = f.getAttribute("source_length")
					susp_off = f.getAttribute("this_offset")
					susp_len = f.getAttribute("this_length")
					obfuscation = f.getAttribute("obfuscation")
					if(obfuscation == "" or obfuscation == None):
						obfuscation = f.getAttribute("type")
					if len(obfuscation) <= 1:
						obfuscation = "NoPlagiarism_Summary"
					self.plag_passages.append([src_off, src_len, susp_off, susp_len, obfuscation, False])

	def compare(self):
		ps=[]
		detections=[]

		# if "1027" not in self.srcDoc and "1376" not in self.suspDoc:
		# 	return
		with open(self.bowDataFile, 'a') as f:
			with open(self.cosDataFile, 'a') as g:
				for c in range(len(self.susp_bow)):
					for r in range(len(self.src_bow)):
						##Initial dice/cosine comparison of BoW in sentence pairs
						cos_mes = cosine_measure(self.susp_bow[c],self.src_bow[r])
						dice_co = dice_coeff(self.susp_bow[c],self.src_bow[r])
						jacc = jaccard(self.susp_bow[c],self.src_bow[r])

						if cos_mes>self.th1 and dice_co>self.th2:
							ps.append((c,r))
						# Export all BoW data to file on format
						# src_offset, susp_offset, cos_mes, dice_co, isPlag
						#
						# Check correct data to determine isPlag(if offset within offset+length)
						# # passage form: (src_off, src_len, susp_off, susp_len, obfsucation)
						plag = "no"
						plagType = "NoPlagiarism"
						potentialPlagType = "NoPlagiarism"

						for passage in self.plag_passages:
							suspRange = range(int(self.susp_offsets[c][0]),int(self.susp_offsets[c][0])+int(self.susp_offsets[c][1]))
							srcRange = range(int(self.src_offsets[r][0]),int(self.src_offsets[r][0])+int(self.src_offsets[r][1]))
							suspPassRange = range(int(passage[2]),int(passage[2])+int(passage[3]))
							srcPassRange = range(int(passage[0]),int(passage[0])+int(passage[1]))
							# Perform pre-proc test without this
							susInt = set(suspRange).intersection(suspPassRange)
							srcInt = set(srcRange).intersection(srcPassRange)
							if susInt:
								if srcInt:
									passSize = len(suspRange)*len(srcRange)
									intSize = len(susInt)*len(srcInt)
									if intSize / passSize > 0.5:
										potentialPlagType = passage[4]
										passage[5] = True
									# print(passage)

									# Backtrack to confirm match
									c_first = c
									while c_first>0 and self.susp_offsets[c_first][0] > int(passage[2]):
										c_first-=1
									r_first = r
									while r_first > 0 and self.src_offsets[r_first][0] > int(passage[0]):
										r_first-=1
									if c - c_first == r - r_first:
										if(len(passage[4])>2):
											plag = "yes"
											plagType = passage[4]
											# print(plagType)
										else:
											plagType = "NoPlagiarism_Summary?"
										# print(plagType)
						src_sent = self.src_text[self.src_offsets[r][0]:self.src_offsets[r][0]+self.src_offsets[r][1]].replace('|', '§')
						susp_sent = self.susp_text[self.susp_offsets[c][0]:self.susp_offsets[c][0]+self.susp_offsets[c][1]].replace('|', '§')
						# if(plagType is not "NoPlagiarism"):
							# print("{},{},{},{},\n{},{},\n|{}|,\n|{}|,\n|{}|\n".format(self.srcDoc, self.suspDoc, c,r, cos_mes, plagType.encode(sys.stdout.encoding, errors='replace'), src_sent.encode(sys.stdout.encoding, errors='replace'), susp_sent.encode(sys.stdout.encoding, errors='replace'),self.src_offsets[r][0]))
						g.write("{},{},{},{},{},{},{},{},|{}|,|{}|,{},{},{},{}\n".format(self.srcDoc, self.suspDoc, c,r, cos_mes, dice_co, potentialPlagType.encode(sys.stdout.encoding,errors='replace'),plagType.encode(sys.stdout.encoding, errors='replace'), "","",self.src_offsets[r][0], self.susp_offsets[c][0],self.src_offsets[r][1],self.susp_offsets[c][1]))
						# if len(str(self.src_offsets[r][0])) < 1:
							# print("{},{},{},{},{},{},{},{},|{}|,|{}|,{},{},{},{}\n".format(self.srcDoc, self.suspDoc, c,r, cos_mes, dice_co, potentialPlagType.encode(sys.stdout.encoding,errors='replace'),plagType.encode(sys.stdout.encoding, errors='replace'), "","",self.src_offsets[r][0], self.susp_offsets[c][0],self.src_offsets[r][1],self.susp_offsets[c][1]))
						# f.write("{},{},{},{},{},{},{},{},{};\n".format(self.src_offsets[r][0], self.susp_offsets[c][0],self.src_offsets[r][1],self.susp_offsets[c][1],cos_mes, dice_co,jacc, plag,plagType))
						# if cos_mes > 0.8 and "0005" in self.suspDoc and "1090" in self.srcDoc:
						# 	print(self.suspDoc)
						# 	print(self.srcDoc)
						# 	print(cos_mes)
						# 	print(plagType)
						# 	print("{} {} - {} {}".format(self.src_offsets[r][0],self.src_offsets[r][1],self.susp_offsets[c][0],self.susp_offsets[c][1]))
						# 	print(self.src_text [self.src_offsets[r][0]: self.src_offsets[r][0] +self.src_offsets[r][1]])
						# 	print(self.susp_text[self.susp_offsets[c][0]:self.susp_offsets[c][0]+self.susp_offsets[c][1]])
		for passage in self.plag_passages:
			if passage[5] == False:
				print("MISSING PASSAGE")
		print("Compared!")
	def postprocess(self):
		pass
	def output(self):
		pass

	def __init__(self, pandir, suspdir,susp, srcdir,src, outdir):
		self.rem_sw = 'no'
		self.src_voc={}
		self.susp_voc={}
		self.src_offsets=[]
		self.susp_offsets=[]
		self.src_sents=[]
		self.susp_sents=[]
		self.min_sentlen=3
		self.rssent='no'
		self.susp = susp
		self.src = src
		self.srcDoc = os.path.join(srcdir, src)
		self.suspDoc = os.path.join(suspdir, susp)
		# if "0029" not in self.suspDoc and "0744" not in self.srcDoc:
		# 	return
		self.outDir = outdir
		self.panDir = pandir
		self.bowDataFile = self.outDir + "\\bowData.csv"
		self.cosDataFile = self.outDir + "\\cosData.csv"

		self.th1 = 0.33
		self.th2 = 0.33
		self.th3 = 0.4
		src_fp = codecs.open(self.srcDoc, 'r', 'utf-8')
		self.src_text = src_fp.read()
		src_fp.close()
		susp_fp = codecs.open(self.suspDoc, 'r', 'utf-8')
		self.susp_text = susp_fp.read()
		susp_fp.close()
		self.preprocess()
		self.compare()
		self.postprocess()
		self.output()

# Parse arguments
if len(sys.argv) == 3:
	t1=time.time()
	panDir = sys.argv[1]
	pairs = sys.argv[1]+"\\pairs"
	srcdir = sys.argv[1]+"\\src"
	suspdir = sys.argv[1]+"\\susp"
	outdir = sys.argv[2]

	lines = open(pairs, 'r').readlines()
	i = 0
	t = time.time()
	start_time = time.time()
	pairs = len(lines)
	print("{} pairs to process".format(pairs))
	for line in lines:
		# print(line)

		susp, src = line.split()

		# if "00143" not in susp or "02739" not in src:
		# 	continue
		print(susp+ " - "+src)
		pd = plagdet(panDir,suspdir,susp, srcdir,src, outdir)
		i+=1
		p = float(i)/pairs
		t = time.time()
		avg_t = (t-start_time) / float(i)
		rem_t = ((pairs-i) * avg_t) / 60
		print("ETA: {0:.2f} minutes - Progress: {1:.2f}%".format(rem_t, p*100))
		print(i)

else:
	print("Expecting arguments: pandir outdir")
	exit()