import random
import csv



random.seed(42) # You know why....	
with open('out_test/stat.csv', 'r') as csvfile:
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')
	with open("out_test/seeds_model.csv", 'a') as mFile:
		mFile.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen"))
		mWriter = csv.writer(mFile, lineterminator='\n')
		with open("out_test/seeds_test.csv", 'a') as tFile:
			tFile.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen"))
			tWriter = csv.writer(tFile, lineterminator='\n')

			pairsName = ""
			pairData = []

			for row in csvr:
				pair = row[0]
				if "Pair" in pair:
					continue
				if pair != pairsName: #New! Write and reset
					# if random.random() > 0.7: #Test data
					tWriter.writerows(pairData)
					# else: # Model data
						# mWriter.writerows(pairData)
					print(pairsName)

					pairData = []
					pairsName = pair
				else:
					pairData.append(row)



'''
with open('oout/stat.csv', 'r') as csvfile:
	csvr = csv.reader(csvfile, delimiter=',', quotechar='|')

	pairs = {}

	for row in csvr:
		pair = row[0]
		if "Pair" in pair:
			continue
		if pair not in pairs:
			pairs[pair] = []
		pairs[pair].append(row)

	random.seed(42) # You know why....

	with open("oout/seeds_model.csv", 'a') as mFile:
		mFile.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen"))
		mWriter = csv.writer(mFile, lineterminator='\n')
		with open("oout/seeds_test.csv", 'a') as tFile:
			tFile.write("{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}\n".format("Pair","SrcSenOff", "SuspSenOff","PlagType","Cos", "IsMaxCos", "NeighbourMaxCos","Dice", "IsMaxDice", "NeighbourMaxDice","PotentialPlagType", "LikelyPlag", "SrcOffset", "SuspOffset", "SrcLen","SuspLen"))
			tWriter = csv.writer(tFile, lineterminator='\n')
			i = 0
			numTotal = len(pairs)
			for pair in pairs:
				if random.random() > 0.7: #Test data
					tWriter.writerows(pairs[pair])
				else: # Model data
					mWriter.writerows(pairs[pair])
				p = float(i)/numTotal
				t = time.time()
				avg_t = (t-start_time) / float(i)
				rem_t = ((numTotal-i) * avg_t) / 60
				print("ETA: {0:.2f} minutes - Progress: {1:.2f}%".format(rem_t, p*100))
'''
